<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Pipeline SPIP autoriser
 */
function acs_autoriser() {
}

/**
 * Fonction appelée par le pipeline SPIP "autoriser"
 */
function autoriser_acs_dist($faire, $type, $id, $qui, $opt) {
	// Si l'utilisateur n'est pas identifie, pas la peine d'aller plus loin
	if (!isset($qui['statut'])) {
		return false;
	}
	$admin = ($qui['statut'] === '0minirezo');
	$webmestre = ($qui['webmestre'] === 'oui');
	$ok = ($admin && $webmestre);
/*	acs_log('($faire="' . $faire .
		'", $type="' . $type .
		'") id_auteur=' . $qui['id_auteur'] .
		' (' . $qui['nom'] . ') webmestre=' . $webmestre . ' admin=' . $admin . ' : ' .
		($ok ? 'ok' : 'niet'), _LOG_INFO);*/
	return $ok;
}
