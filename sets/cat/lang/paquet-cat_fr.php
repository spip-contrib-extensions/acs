<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'cat_nom' => 'Cat',
	'cat_slogan' => 'Composants ACS',
	'cat_description' => 'Jeu de composants configurables',
	'theme_def' => 'Par défaut',
	'theme_def_tip' => 'Thème par défaut. Trois colonnes.',
	'theme_trois_colonnes' => 'Trois colonnes',
	'theme_trois_colonnes_tip' => 'Trois colonnes, sans onglets',
];
