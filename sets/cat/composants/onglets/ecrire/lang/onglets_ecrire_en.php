<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// Composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Panels',
'description' => 'Panels - Navigation menu',
'help' => 'Panels titles and tooltips translations are in the component lang file.',

'FondColor' => 'Background',
'BordColor' => 'Border',
'CouleurInactif' => 'Inactive',
'CouleurSurvol' => 'Hover',
'LienInactif' => 'Inactive',
'Link' => 'Link',
'LinkHover' => 'Hover',
'Space' => 'Space between two panes',
'Publier' => 'Panel "Publish"',
'Liens' => 'Panel "Links"',
'1' => 'Panel 1',
'2' => 'Panel 2',
'3' => 'Panel 3',
'4' => 'Panel 4',
'5' => 'Panel 5'
];
