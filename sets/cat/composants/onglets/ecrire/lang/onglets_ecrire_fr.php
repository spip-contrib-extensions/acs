<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// Composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Onglets',
'description' => 'Menu de navigation par onglets',
'help' => 'Les traductions des intitulés des onglets sont dans le fichier de langue du composant Onglets.',

'FondColor' => 'Fond',
'BordColor' => 'Bordure',
'CouleurInactif' => 'Inactif',
'CouleurSurvol' => 'Au survol',
'LienInactif' => 'Inactif',
'Link' => 'Lien',
'LinkHover' => 'Au survol',
'Space' => 'Espace entre deux onglets',
'Publier' => 'Onglet "Publier"',
'Liens' => 'Onglet "Liens"',
'1' => 'Onglet 1',
'2' => 'Onglet 2',
'3' => 'Onglet 3',
'4' => 'Onglet 4 ',
'5' => 'Onglet 5 '
];
