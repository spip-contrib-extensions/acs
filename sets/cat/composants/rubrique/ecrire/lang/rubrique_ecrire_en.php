<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Section',
'description' => 'Section of the site',
'info' => 'Display logos, title, description, content, and joined documents of a section.',

'help' => 'Section\'s page can depend on a keyword : then, this page contain the instance of Cadre component nn where nn is defined in the keyword description field by a balise &lt;nic-page=nn&gt;.
To avoid displaying this keywoird\' group on the public site, its title must begin by a "_".',

'MaJ' => 'last update',
'LogoAlign' => 'Logo alignment',
'TitreAlign' => 'Title alignment',
'Article' => 'Replace by article if unique',
'Souligne' => 'Underline title with lgo dominant color',
'StylePage' => 'Page style'
];
