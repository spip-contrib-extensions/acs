<?php

$keywords = [
	'_Storage' => [
		'descriptif' => 'Archivage of categories',
		'texte' => 'Categories with a keyword beginning by "_" are not displmayed in lists of categories.',
		'tables_liees' => 'rubriques',
		'mots' => [
			'_Archive' => [
				'descriptif' => 'Archive',
				'texte' => 'Archives'
			]
		]
	]
];
