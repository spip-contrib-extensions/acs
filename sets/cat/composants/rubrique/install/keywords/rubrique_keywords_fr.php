<?php

$keywords = [
	'_Archivage' => [
		'descriptif' => 'Archivage de rubriques',
		'texte' => 'Les rubriques avec un mot-clé qui commence par  "_" n\'apparaissent plus dans les listes de rubriques.',
		'tables_liees' => 'rubriques',
		'mots' => [
			'_Archive' => [
				'descriptif' => 'Rubrique archivée',
				'texte' => 'Archives'
			]
		]
	]
];
