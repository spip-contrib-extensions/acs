<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Sites',
'description' => 'Liste de sites webs.',
'help' => 'référencés dans SPIP',

'NbLettres' => 'Lettres avant coupure',
'NbLettresHelp' => '-1 pour ne pas couper le texte.',
];
