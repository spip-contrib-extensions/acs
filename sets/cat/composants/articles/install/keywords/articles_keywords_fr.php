<?php

$keywords = [
	'_Archivage' => [
		'descriptif' => 'Archivage d\'articles',
		'texte' => 'Les articles avec un mot-clé qui commence par  "_" n\'apparaissent plus dans les listes d\'articles.',
		'tables_liees' => 'articles',
		'mots' => [
			'_Archive' => [
				'descriptif' => 'Archive',
				'texte' => 'Article archivé'
			]
		]
	]
];
