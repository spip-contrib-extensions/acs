<?php

$keywords = [
	'_Storage' => [
		'descriptif' => 'Archived articles',
		'texte' => 'Articles with a keyword beginning by  "_" are not displayed in articles lists.',
		'tables_liees' => 'articles',
		'mots' => [
			'_Archive' => [
				'descriptif' => 'Archive',
				'texte' => 'Archived article'
			]
		]
	]
];
