<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Articles',
'description' => 'Lists of articles.',
'info' => 'Look of articles lists',

'Bord' => 'Border top',
'MargeBas' => 'Margin bottom',
'NbLettres' => 'Nb of letters before cut',
'LogoTailleMax' => 'Max logo size',
'Pagination' => 'Articles per page'
];
