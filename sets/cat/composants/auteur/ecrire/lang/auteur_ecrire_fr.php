<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// ACS component specific - spécifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Auteur',
'description' => 'Page d\'un auteur avec ses <i>n</i> derniers articles.',
'info' => 'Ajax & soft-downgrade',

'NbArticles' => 'Nombre d\'articles par page'
];
