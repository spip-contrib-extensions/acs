<?php

// This is a SPIP-ACS language file	-	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Banner',
'description' => 'Banner changing on mouse over, plus optionnal website logo.',
'help' => 'On mouse over, Banner randomly display one of the available image uploaded.
Without any banner image, website name is displayed instead.
A banner measure normally 468x60 pixels.',

'Image' => 'Banner',
'Hauteur' => 'Height',
'Logo' => 'Display website logo',
'LogoMargeV' => 'Vertical margin',
'LogoMargeH' => 'Horizontal margin',
'TextColor' => 'Text color',
'TextColorOver' => 'Over',
];
