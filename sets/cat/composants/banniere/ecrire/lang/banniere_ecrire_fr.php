<?php

// This is a SPIP-ACS language file	-	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Banni&egrave;re',
'description' => 'Banni&egrave;re qui change au survol, et logo du site, en option.',
'help' => 'Au survol du pointeur, la bannière affiche alatoirement l\'une des images disponibles.
Le nom du site est affich&eacute; si aucune image n\'a &eacute;t&eacute; t&eacute;l&eacute;vers&eacute;e sur le serveur.
Une banni&egrave;re &quot;standard&quot; mesure 468x60 pixels.',

'Image' => 'Banni&egrave;re',
'Hauteur' => 'Hauteur',
'Logo' => 'Afficher le logo du site',
'LogoMargeV' => 'Marge verticale',
'LogoMargeH' => 'Marge horizontale',
'TextColor' => 'Couleur texte',
'TextColorOver' => 'Au survol',
];
