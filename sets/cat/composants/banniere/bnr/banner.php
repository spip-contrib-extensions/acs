<?php

// Doit renvoyer UNIQUEMENT une image

$url = $GLOBALS['ACS_CHEMIN'] . '/banniere/';
// la variable globale est rendue disponible en appellant ce fichier par un wrap

require_once('banner_lib.php');

$banners = get_banners($url, $_GET['exclut']);

$bnrid = rand(0, count($banners) - 1);
if (isset($banners[$bnrid])) {
	header('Location: ' . $url . "$banners[$bnrid]");
}
else {
	header('Location: ' . $url . $_GET['exclut']);
}
die;
