<?php

/* get_banners function read banners files names.
 *
 */

function get_banners($path = '.', $exclut = '') {
	$filetypes = ['gif', 'jpg', 'png', 'jpeg'];
	$r = [];
	foreach ($filetypes as $type) {
		$r = array_merge($r, explore_path($path, $type, $exclut));
	}
	return $r;
}

function explore_path($folder = '.', $filetype = '', $exclut = '') {
		$currdir = getcwd();
		if ($folder && @is_dir("$currdir/$folder")) {
			chdir("$currdir/$folder");
		}
		$dh = opendir('.');
		$a_files = [];
		while (false !== ($file = readdir($dh))) {
			// insert all the files in an array
			if (is_file($file) && (strtoupper(substr($file, (-1 * strlen($filetype)))) == strtoupper($filetype)) && ($file != $exclut)) {
				$a_files[] = $file;
			}
			if (@is_dir($file) && $file[0] != '.') {
				$a_files[$file] = explore_path($file, $filetype);
			}
		}
		closedir($dh);
		chdir($currdir);
 //		if (is_array($a_files))
 //				array_multisort($a_files);
	return $a_files;
}
