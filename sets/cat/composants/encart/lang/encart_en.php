<?php

// This is a SPIP language file
// ACS component specific

$GLOBALS[$GLOBALS['idx_lang']] = [
'pause' => 'Pause',
'play' => 'Play',
'suivant' => 'Next',
'precedent' => 'Previous',
];
