<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// ACS component specific - spécifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'pause' => 'Pause',
'play' => 'Jouer',
'suivant' => 'Suivant',
'precedent' => 'Pr&eacute;c&eacute;dent',
];
