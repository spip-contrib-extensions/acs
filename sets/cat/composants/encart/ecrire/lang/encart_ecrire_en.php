<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Encart',
'description' => 'Blocks of last published documents, articles or categories published with selected keyword.',
'Type' => 'Type',
'TypeArticles' => 'articles',
'TypeRubriques' => 'sections',
'SouligneRub' => 'underline section',
'SouligneRubHeight' => 'Section underline height'
];
