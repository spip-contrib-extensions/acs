<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Encart',
'description' => 'Encart des n derniers documents, articles, ou rubriques publiés avec le mot-clef choisi.',
'Type' => 'Type',
'TypeArticles' => 'articles',
'TypeRubriques' => 'rubriques',
'SouligneRub' => 'Souligner la rubrique',
'SouligneRubHeight' => 'Hauteur du soulignement'
];
