<?php

$keywords = [
	'Articles spéciaux' => [
		'descriptif' => 'Articles mis en exergue',
		'texte' => 'Articles affichés dans une instance de composant Encart.',
		'tables_liees' => 'articles',
		'mots' => [
			'Edito' => [
				'descriptif' => 'Éditorial',
				'texte' => 'Article affiché en éditorial'
			],
			'Encart' => [
				'descriptif' => 'Encart',
				'texte' => 'Article affiché en encart'
			]
		]
	]
];
