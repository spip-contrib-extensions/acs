<?php

$keywords = [
	'Highlighted articles' => [
		'descriptif' => 'Highlighted articles',
		'texte' => 'Articles displayed in one instance of composant Encart.',
		'tables_liees' => 'articles',
		'mots' => [
			'Edito' => [
				'descriptif' => 'Editorial',
				'texte' => 'Article displayed as editorial'
			],
			'Encart' => [
				'descriptif' => 'Insert',
				'texte' => 'Article displayed in insert'
			]
		]
	]
];
