<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'janvier' => 'Janvier',
'fevrier' => 'F&eacute;vrier',
'mars' => 'Mars',
'avril' => 'Avril',
'mai' => 'Mai',
'juin' => 'Juin',
'juillet' => 'Juillet',
'aout' => 'Ao&ucirc;t',
'septembre' => 'Septembre',
'octobre' => 'Octobre',
'novembre' => 'Novembre',
'decembre' => 'D&eacute;cembre',
'di' => 'di',
'lu' => 'lu',
'ma' => 'ma',
'me' => 'me',
'je' => 'je',
've' => 've',
'sa' => 'sa',
'publie' => 'Articles publi&eacute;s',
'modifie' => 'Articles modifi&eacute;s'
];
