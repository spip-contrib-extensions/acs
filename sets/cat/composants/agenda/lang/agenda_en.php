<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'janvier' => 'January',
'fevrier' => 'February',
'mars' => 'March',
'avril' => 'April',
'mai' => 'May',
'juin' => 'June',
'juillet' => 'July',
'aout' => 'August',
'septembre' => 'September',
'octobre' => 'October',
'novembre' => 'November',
'decembre' => 'December',
'di' => 'su',
'lu' => 'mo',
'ma' => 'tu',
'me' => 'we',
'je' => 'th',
've' => 'fr',
'sa' => 'sa',
'publie' => 'Published',
'modifie' => 'Modified'
];
