<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Agenda',
'description' => 'Mini-calendar with dhtml tooltips and links to articles and to news',
'help' => 'Agenda can also show changes.',

'ThisMonth' => 'This month',
'NotThisMonth' => 'Not this month',
'ThisDay' => 'This day',
'BulleFondColor' => 'Tooltip',
'BulleVoirArticlesModifies' => 'Display changes',
'BulleArticleModifieFondColor' => 'Modified text',
'BulleBreveFondColor' => 'News',
'BulleSurvol' => 'Over',
'HeadFondColor' => 'Header'
];
