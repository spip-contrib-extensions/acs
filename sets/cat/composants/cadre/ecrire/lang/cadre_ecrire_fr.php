<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Cadre',
'description' => 'Cadre permet de définir des propri&eacute;t&eacute;s pour un cadre et d\'y glisser d\'autres composants.',
'info' => 'L\'instance Cadre (sans numéro d\'instance) définit les propriétés des menus latéraux sur petits écrans et ne contient pas d\'autres cadres.',
'FondImage' => 'Arri&egrave;re-plan',
'FondImageRepeatX' => 'R&eacute;peter l\'image de fond en X',
'FondImageRepeatY' => 'R&eacute;peter l\'image de fond en Y',
'Orientation' => 'Orientation',
'OrientationVertical' => 'verticale',
'OrientationHorizontal' => 'horizontale',
'Class' => 'Classe CSS',
'ClassHelp' => 'Classe(s) CSS
catExpand: agrandit le Cadre à la hauteur du plus haut des enfants de son parent
catHideLeftOnSmall, catHideRightOnSmall, catExpandOnSmall: changement d\'apparence sur les petits écrans',
'PaddingLeft' => 'gauche',
'PaddingRight' => 'droit',
'PaddingTop' => 'haut',
'PaddingBottom' => 'bas',
'WidthHelp' => 'Laisser vide pour une largeur de 100%',
'HeightHelp' => 'Laisser vide pour une hauteur de 100%'
];
