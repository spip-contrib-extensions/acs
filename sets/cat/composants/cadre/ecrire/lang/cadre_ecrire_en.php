<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Cadre',
'description' => 'Box and container for other components.',
'info' => 'Cadre define a box AND contain other components.

Cadre (without instance number) define lateral menus on small screens and do not contain other cadres.',

'Image' => 'Banner',
'FondImage' => 'Background',
'FondImageRepeatX' => 'Repeat background image on X axis',
'FondImageRepeatY' => 'Repeat background image on Y axis',
'orientation' => 'Orientation',
'vertical' => 'vertical',
'horizontal' => 'horizontal',
'Class' => 'CSS class',
'ClassHelp' => 'CSS class(es)
catExpand: expand Cadre at the hight of his parent\'s tallest children
catHideLeftOnSmall, catHideRightOnSmall, catExpandOnSmall: responsive design on small screens',
'PaddingLeft' => 'left',
'PaddingRight' => 'right',
'PaddingTop' => 'top',
'PaddingBottom' => 'bottom',
'WidthHelp' => 'Keep empty to get 100%'
];
