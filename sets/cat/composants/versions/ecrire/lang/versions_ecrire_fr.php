<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Versions',
'description' => 'Versions d\'un article.',
'help' => 'Affiche les r&eacute;visions successives d\'un article.'
];
