<?php

/**
 * Affiche le diff d'un objet
 * Ex: [<small> (#ID_ARTICLE|affiche_diff{article,#ID_VERSION,diff}|supprimer_tags|couper{50})</small>]
 *
 * @param int $id_objet
 * @param string $objet
 * @param int $id_version
 * @param string $format [optional]
 * @return string html code
 */
function affiche_diff($id_objet, $objet, $id_version, $format = 'complet', $id_diff = null) {
	include_spip('inc/suivi_versions');

	$textes = revision_comparee($id_objet, $id_version, $format, $id_diff);
	foreach ($textes as $champ => $texte) {
		$textes[$champ] = propre_diff(PtoBR($texte));
	}
	$contexte = [
			'id_article' => $id_objet,
			'id_version' => $id_version,
			'lang' => $GLOBALS['spip_lang']
	];
	$contexte = array_merge($contexte, $textes);
	return recuperer_fond('composants/versions/inc-article_version', $contexte);
}

/**
 * Affiche le nom de l'auteur à partir de son id_auteur
 *
 * @param object $auteur
 * @return string auteur
 */
function affiche_auteur_diff($auteur) {
	// Si c'est un nombre, c'est un auteur de la table spip_auteurs
	if ($auteur == intval($auteur) and $s = sql_getfetsel('nom', 'spip_auteurs', 'id_auteur=' . intval($auteur))) {
		return typo($s);
	} else {
		return $auteur;
	}
}
