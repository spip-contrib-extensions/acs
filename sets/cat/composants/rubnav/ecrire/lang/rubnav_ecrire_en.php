<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Rubnav',
'description' => 'Navigation by rubrics.',
'info' => 'Rubrics types:<br>_Group1<br>_Group2<br>_Group3<br>_Ours<br>_Archive<br>',
'help' => 'Some keywords determine how rubrics are displayed:<br>Rubrics with keyword _Group1, _Group2, or _Group3 displays in seperate menus blocks. Rubrics without rubric type keyword are displayed in a last block.<br><br>Rubric with keyword _Ours do not display in navigation menus, but is linked from the Ours component.<br><br>Sections with a keyword beginning by "_" are not displayed in by-sections navigation, but appear in site map.',

'Fond2' => 'level 2',
'Fond3' => 'level 3',
'Fond4' => 'level 4',
'Fond5' => 'level 5',
'Fond6' => 'level 6',
'FondHover' => 'Background, over',
'TitreFond' => 'Title',
'Sep' => 'S&eacute;parator',
'Titre' => 'Show "Sections" as bloc title',
'MaJ' => 'Display last updates',
'DeplierHaut' => 'Wrap',
'DeplierHautOver' => 'over',
'DeplierHaut_rtl' => 'Wrap, right to left',
'DeplierHautOver_rtl' => 'over',
'DeplierBas' => 'Unwrap',
'DeplierBasOver' => 'over'
];
