<?php

$keywords = [
	'_Type de rubrique' => [
		'descriptif' => 'Menu rubnav',
		'texte' => 'Menu de navigation par rubriques.',
		'tables_liees' => 'rubriques',
		'mots' => [
			'_Group1' => [
				'descriptif' => 'Rubrique du groupe 1',
				'texte' => ''
			],
			'_Group2' => [
				'descriptif' => 'Rubrique du groupe 2',
				'texte' => ''
			],
			'_Group3' => [
				'descriptif' => 'Rubrique du groupe 3',
				'texte' => ''
			],
		]
	]
];
