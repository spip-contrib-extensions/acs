<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// ACS component specific - sp&eacute;cifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'stop' => 'Stop',
'play' => 'Play / pause',
'prev' => 'Previous',
'next' => 'Next',
'ffwd' => 'Fast forward',
'frwd' => 'Fast rewind',

'playlist' => 'Playlist',
'download' => 'Download',
'article' => 'Read',
'lire_article' => 'Read article with these audio file',
'err_liste_vide' => 'Empty playlist. No mp3 documents found.'
];
