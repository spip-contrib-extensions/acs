<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP
// ACS component specific - sp&eacute;cifique au composant ACS

$GLOBALS[$GLOBALS['idx_lang']] = [
'stop' => 'Stop',
'play' => 'Lecture / pause',
'prev' => 'Pr&eacute;c&eacute;dent',
'next' => 'Suivant',
'ffwd' => 'Avance rapide',
'frwd' => 'Retour rapide',

'playlist' => 'Playlist',
'download' => 'T&eacute;l&eacute;charger',
'article' => 'Lire',
'lire_article' => 'Lire l\'article contenant ce fichier audio',
'err_liste_vide' => 'Liste de lecture vide. Aucun document mp3 trouv&eacute;.'
];
