<?php

function joli_titre($titre) {
	$titre = basename($titre);
	$titre = preg_replace('/.mp3/', '', $titre);
	$titre = preg_replace('/^ /', '', $titre);
	$titre = preg_replace('/_/i', ' ', $titre);
	$titre = preg_replace("/'/i", ' ', $titre);

	return $titre ;
}
