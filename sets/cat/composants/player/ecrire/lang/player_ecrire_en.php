<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Player',
'description' => 'Audio/video player and playlist',
'help' => 'Playlist display list of last published mp3 documents.
To insert player to an article, just add <b>player</b> to the usual SPIP tag for documents : &lt;docxxx|<b>player</b>&gt;.',

'Btn' => 'Buttons',
'BtnOver' => 'au survol',
'FondPlayer' => 'Player background',
'TextShadow' => 'Text and buttons shadow',
'ProgressBar' => 'Progress bar',
'TrackStatus' => 'Track infos',

'TitreFond' => 'Title',
'ArticleColor' => 'Link to article',
'ArticleColorHover' => 'over',
'PodcastColor' => 'Podcast link',
'PodcastColorHover' => 'over',
'NbMp3' => 'Nb of mp3 files',
'Sep' => 'Separator',
'Mp3on' => 'Active track background',
'Mp3hover' => 'over',
'lmin' => 'Min width',
'lmax' => 'Max width',

'Download' => 'Download link',
];
