<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Lecteur multimédia',
'description' => 'Lecteur sons et vidéos et liste de lecture (playlist).',
'help' => 'La liste de lecture affiche la liste des derniers documents de type mp3 publiés.
Pour insérer le lecteur mp3 dans un article, il suffit d\'ajouter <b>player</b> à la balise SPIP habituelle d\'insertion d\'un document dans un article : &lt;docxxx|<b>player</b>&gt;.',

'Btn' => 'Boutons',
'BtnOver' => 'au survol',
'FondPlayer' => 'Fond lecteur',
'TextShadow' => 'Ombre texte et boutons',
'ProgressBar' => 'Barre de progression',
'TrackStatus' => 'Infos piste',

'TitreFond' => 'Titre',
'ArticleColor' => 'Lien vers l\'article',
'ArticleColorHover' => 'au survol',
'PodcastColor' => 'Lien podcast',
'PodcastColorHover' => 'au survol',
'NbMp3' => 'Nb de morceaux',
'Sep' => 'S&eacute;parateur',
'Mp3on' => 'Fond piste active',
'Mp3hover' => 'au survol',
'lmin' => 'Largeur mini',
'lmax' => 'Largeur maxi',

'Download' => 'Lien de t&eacute;l&eacute;chargement',
];
