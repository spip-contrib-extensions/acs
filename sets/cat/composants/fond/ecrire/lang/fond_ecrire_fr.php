<?php

// This is a SPIP-ACS language file	--	Ceci est un fichier langue de SPIP-ACS

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Fond',
'description' => 'Fond de page. Image ou couleur de fond pour toutes les pages du site.',
'info' => 'Définit le style général du site et les valeurs par d&eacute;faut d\'autres composants.',
'help' => 'Le contenu des pages du site est défini avec des composants Cadre.

La taille des petits liens de navigation s\'applique aux paginations, aux notes, aux post-scriptums et &agrave; tous les petits &eacute;l&eacute;ments de navigation.

 * taille typo
 * Base			 100%
 * -------------------------------------
 * 20px			 1.27em
 * 19px			 1.21em
 * 18px			 1.15em
 * 17px			 1.09em
 * 16px			 1.03em
 * 15px			 0.96em
 * 14px			 0.88em
 * 13px			 0.82em
 * 12px			 0.77em
 * 11px			 0.71em
 * 10px			 0.65em
 * 9px			 0.59em
 * Attention : en dessous de 0.71em le texte devient illisible en text smallest sur MSIE
',
'Alert' => 'Message d\'alerte',
'Favicon' => 'Ic&ocirc;ne de signet',
'FaviconHelp' => 'S\'affiche dans la barre d\'adresse et les titres d\'onglets de l\'explorateur.',
'Color' => 'Fond',
'ImageHelp' => 'Image de fond pour toutes les pages du site. Il faut définir une couleur par défaut pour que l\'image de fond soit prise en compte.',
'ImageRepeatX' => 'R&eacute;peter l\'image de fond en X',
'ImageRepeatY' => 'R&eacute;peter l\'image de fond en Y',
'NavSize' => 'Taille des petits liens de navigation',
'BlocSize' => 'Taille des blocs',
'ImgMax' => 'Taille max des images',
'ImgMaxHelp' => 'Limite la taille des images.',

'TitleSize' => 'Taille de titre',
'STSize' => 'Taille de sous-titre',

'Spacer' => 'Espace par d&eacute;faut entre les blocs',

'PoesieFont' => 'Fonte &lt;poesie&gt;',
'PoesieFontFamily' => 'Famille de fonte',
'PoesieFontSize' => 'Taille',

'table' => 'Tableaux',
'TabBordColor' => 'Bordure',
'TabFirst' => 'Premi&egrave;re ligne',
'TabOdd' => 'Ligne paire',
'TabEven' => 'Ligne impaire',
];
