<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Background',
'description' => 'Background image or color for all pages.',
'info' => 'Define general website look and feel and default values for some other components.',
'help' => 'Website content is defined with Cadre components.

Size for little navigation links set the size of pagination, notes, post-scriptums and all small navigation elements.

 * typo size
 * Base			 100%<br>
 * -------------------------------------<br>
 * 20px			 1.27em
 * 19px			 1.21em
 * 18px			 1.15em
 * 17px			 1.09em
 * 16px			 1.03em
 * 15px			 0.96em
 * 14px			 0.88em
 * 13px			 0.82em
 * 12px			 0.77em
 * 11px			 0.71em
 * 10px			 0.65em
 *	9px			 0.59em
 * Caution : under 0.71em texte is unreadable in text smallest on MSIE
',

'Alert' => 'Alert message',
'Favicon' => 'Favicon',
'Favicon_help' => 'Displayed in address bar and navigator panes.',
'Color' => 'Background',
'ImageHelp' => 'Background image. Default color must be defined to enable the background image.',
'ImageRepeatX' => 'Repeat background image on X axis',
'ImageRepeatY' => 'Repeat background image on Y axis',
'NavSize' => 'Size for little navigation links',
'BlocSize' => 'Size for blocks',
'ImgMax' => 'Max size for images',
'ImgMaxHelp' => 'Limit images size.',

'TitleSize' => 'Title size',
'STSize' => 'Subtitle size',

'Spacer' => 'Default space between blocks',

'PoesieFont' => 'Font for &lt;poesie&gt;',
'PoesieFontFamily' => 'Font family',
'PoesieFontSize' => 'Size',

'table' => 'Tables',
'TabBordColor' => 'Border',
'TabFirst' => 'First line',
'TabOdd' => 'Odd',
'TabEven' => 'Even',
];
