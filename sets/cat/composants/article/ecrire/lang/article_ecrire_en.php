<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = [

'nom' => 'Article',
'description' => 'Article',
'info' => 'Look of an article',

'StylePage' => 'Page style',
'StylePageHelp' => 'Article page can depend on a keyword from a keyword group choosed here.',

'Bord' => 'Border top',
'LogoTailleMax' => 'Max logo size',

'Dates' => 'Dates',
'Aut' => 'Authors',
'ChapoGras' => 'Set the header\'s font weight to bold',
'Stats' => 'Visits',
'LogoTailleMaxHelp' => 'Logo max size. Logo is not displayed if the value is not numeric.',
'Added' => 'Added since last revision',
'Deleted' => 'Deleted since last revision'

];
