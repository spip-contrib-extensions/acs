<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Ours',
'description' => 'Informations sur le site.',
'info' => 'Si une rubrique a le mot-cl&eacute; _Ours, un lien vers cette rubrique est affich&eacute;.',
'help' => 'Une rubrique avec le mot-cl&eacute; _Ours sert à rassembler vos informations &eacute;diteur.<br>',

'Text' => 'Texte',
'Link' => 'Liens',
'LinkHover' => 'Au survol',
'Custom' => 'Pied de page personnalis&eacute; : ',

'LienCopyright' => 'Copyright',
'LienResume' => 'R&eacute;sum&eacute; en mode texte',
'LienACS' => 'ACS',
'LienSPIP' => 'SPIP',
'LienRSS' => 'RSS',
'Stats' => 'Statistiques du site'
];
