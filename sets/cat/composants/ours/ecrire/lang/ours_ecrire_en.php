<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'nom' => 'Ours',
'description' => 'Website informations.',
'info' => 'If a rubric has the keyword _Ours, a link to these rubric is displayed.',
'help' => 'A rubric with keyword _Ours is used to group your editor\'s informations.',

'Text' => 'Text',
'Link' => 'Links',
'LinkHover' => 'Over',
'Custom' => 'Custom footer text : ',

'LienCopyright' => 'Copyright',
'LienResume' => 'Text abstract',
'LienACS' => 'ACS',
'LienSPIP' => 'SPIP',
'LienRSS' => 'RSS',
'Stats' => 'Website statistics'
];
