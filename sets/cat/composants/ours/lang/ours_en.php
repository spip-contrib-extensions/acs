<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
'text' => 'Text mode abstract',
'text_tooltip' => 'Low speed connection',
'refus' => 'Refused articles',
'proposes' => 'Submitted articles',
'home' => 'back to home',
'acs_powered_tip' => 'Community website for Site Configuration Wizard',

'articles_refuses' => 'Refused articles are visible here.',

'visites' => '@nb_articles@ texts from @nb_auteurs@ authors read @visites_articles@ times by @total_visites@ persons.'

];
