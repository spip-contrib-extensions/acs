<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function upgrade_set($from, $to) {
	if (version_compare($from, '3.3.0', '>')) {
		return;
	}

	$cv = composants_variables();
	$acs_vars = [];

	$v2del = ['acsSet', 'acsArticleLock', 'acsOngletsLeft', 'acsOngletsRight', 'acsRubriqueLock'];

	// Read all ACS variables
	foreach ($GLOBALS['meta'] as $vn => $val) {
		if (substr($vn, 0, 3) === 'acs') {
			$acs_vars[$vn] = $val;
		}
	}
	// Unstore regular variables from used components and instances
	foreach ($cv as $c => $p) {
		foreach (array_keys($p['vars']) as $var) {
			unset($acs_vars['acs' . ucfirst($c) . $var]);
		}
		foreach (composant_instances($c) as $nic) {
			foreach ($p['vars'] as $var => $vp) {
				unset($acs_vars['acs' . ucfirst($c) . $nic . $var]);
			}
		}
	}

	if (count($acs_vars) > 0) { // Remaining ghost variables
		$styles2var = [
			'margin-top' => 'Top',
			'margin-bottom' => 'Bottom',
			'margin-left' => 'Left',
			'margin-right' => 'Right',
			'width' => 'Width',
		];
		foreach ($acs_vars as $vn => $val) {
			// Case of old variables 1style, 2style, ...
			if ((substr($vn, 0, 8) == 'acsCadre') && (substr($vn, -5) == 'style')) {
				$vid = substr(substr($vn, -6), 0, 1);
				$oldcid = substr(substr($vn, 8), 0, -6);
				$wval = acs_get('acsCadre' . $oldcid . $vid);
				if (substr($wval, 0, 6) === 'cadre-') {
					$newcid = substr($wval, 6);
					$rval = var_recursive($GLOBALS['meta'], $vn); // Real value;
					if (substr($val, 0, 1) === '=') {
						$newvalcid = substr(acs_get('acsCadre' . substr(substr($val, 9), 0, -5)), 6);
					}
					$newval = '';
					foreach ($styles2var as $style => $varname) {
						if (preg_match('/' . preg_quote($style) . '\s*\:\s*([^;\}]*)/', $rval, $m)) {
							$newval = ($newvalcid && ($newcid !== $newvalcid) ? '=acsCadre' . $newvalcid . $varname : $m[1]);
							$newvn = 'acsCadre' . $newcid . $varname;
							acs_set($newvn, $newval);
							$v2del[] = $vn;
							$r .= ' set <b>' . $newvn . '</b>=<b>' . $newval . '</b> from ' . $vn . '<br>';
						}
					}
				}
			} elseif (substr($vn, 0, 7) == 'acsTags') { // variables from Tags (component suppressed in 3.3.0)
				$v2del[] = $vn;
			}
		}
		if (count($v2del)) {
			$r .= '<br><b>Deleted some unused variables</b>: ';
			foreach ($v2del as $var) {
				acs_unset($var);
				$r .= $var . ' ';
				unset($acs_vars[$var]);
			}
		}
	}
	if (count($acs_vars)) {
		$r .= '<br><br><b>Ghosts variables NOT processed automatically</b>: ' . dbg($acs_vars, 'html');
	}
	return '<br>Upgrade from ' . $from . ' to ' . $to . '<br><br>' . $r;
}
