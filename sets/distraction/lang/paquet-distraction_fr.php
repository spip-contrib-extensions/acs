<?php

// This is a SPIP language file	--	Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
		'distraction_nom' => 'DISTraction',
		'distraction_slogan' => 'Squelettes par défaut de SPIP',
		'distraction_description' => 'Utiliser les squelettes par défaut.'
];
