<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function controleurs_var_dist($regs) {
	global $spip_lang;
	include_spip('inc/filtres');
	require_once(_DIR_ACS . 'inc/composant/classControles.php');
	require_once(_DIR_ACS . 'inc/acs_vue.php');

	$cv = composants_variables();
	list(, $crayon, $type, $champ, $id) = $regs;
	$v = explode('_', $champ);
	$c = $v[0]; // composant
	$v = $v[1]; // variable
	$val = acs_get('acs' . ucfirst($c) . ($id ? $id : '') . $v);
	$type = $cv[$c]['vars'][$v]['type'];

	// il faut passer champ=>source pour les comparaisons dans action/crayons_store
	$crayon = new SecureCrayon('var-' . $champ . '-' . $id, [$champ => $val]);

	$ctl_name = 'ctl' . ucfirst($type);
	if (class_exists($ctl_name)) {
		$ctl = new $ctl_name($c, ($id ? $id : ''), $v, $val, $cv[$c]['vars'][$v]);
	}
	else {
		// On dessine un controle Use si le type est inconnu (cas des composants sans fichier composant.xml, notamment)
		$ctl = new ctlUse($c, ($id ? $id : ''), $v, $val, $cv[$c]['vars'][$v]);
	}
	$html = acs_vue('acsVar', [
		'width' => $crayon->w - 2,
		'fontsize' => _request('font-size'),
		'oldval' => htmlentities($val),
		'type' => $type,
		'control' => $ctl->draw(),
		'boutons' => crayons_boutons(),
		'crayon_code' => $crayon->code(),
	]);
	return [$html, null];
}
