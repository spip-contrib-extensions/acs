<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Réaffiche le composant à crayonner, avec son éditeur
 * Le crayon peut passer un id d'article, de rubrique, de mot-clé, ou de groupe
 * de mot-clefs sous la forme: id_article-45 dans la classe du pinceau d'un
 * composant dépendant de l'article ou de la rubrique.
 *
 * @param array $regs
 * @return array (html, status)
 */
function controleurs_composant_dist($regs) {
	global $spip_lang;

	include_spip('public/assembler');
	require_once(_DIR_ACS . 'inc/acs_vue.php');
	require_once(_DIR_ACS . 'inc/composant/classAdminComposant.php');
	require_once(_DIR_ACS . 'inc/acs_widgets.php');

	list(,$crayon, $type, $class, $id) = $regs;

	$c = $class . '/' . $class;
	$crayon = new SecureCrayon("composant-$class-$id");

	$composant = new AdminComposant($class, $id);
	$icon = find_in_path('composants/' . $class . '/images/' . $class . '_icon.gif');
	if (($crayon->left + $crayon->largeur) > ($crayon->ww / 2)) {
		$left = ($crayon->left < 300) ? 0 : - 300;
	}
	else {
		$left = Min($crayon->ww / 2, $crayon->left + $crayon->largeur);
	}
	$contexte = [
		'c' => 'composants/' . $c,
		'nic' => $id,
		'lang' => (_request('lang') ? _request('lang') : $GLOBALS['spip_lang'])
	];
	$css_class = _request('class'); /* classe du crayon */
	$ctxhtml = '';
	$contextes = [
		'id_article' => '/\bid_article-(\d+)\b/',
		'id_rubrique' => '/\bid_rubrique-(\d+)\b/',
		'id_mot' => '/\bid_mot-(\d+)\b/',
		'id_groupe' => '/\bid_groupe-(\d+)\b/',
		'recherche' => '/\brecherche-(\w+)\b/',
		'page' => '/\bpage-(\w+)\b/'
	];
	$matches = [];
	foreach ($contextes as $c => $re) {
		if (preg_match($re, $css_class, $matches) > 0) {
			$contexte[$c] = $matches[1];
			$ctxhtml .= '<input type="hidden" name="context_' . $c . '" value="' . $contexte[$c] . '" />';
		}
	}
	$titre = ucfirst($composant->class) . ($id == 0 ? '' : ' ' . $id);
	$html = '<div style="width:' . $crayon->w . 'px; height:' . $crayon->h . 'px">' .
		'<div id="' . "composant-$class-$id" .
			'" style="position: absolute; border: 2px outset #fddf00; top: -1px;left: -1px;opacity: 0.98; width:' .
			$crayon->w . 'px; height:' .
			$crayon->h . 'px; font-size:' .
			_request('em') . '">' .
			recuperer_fond('vues/composant', $contexte) .
		'</div>' .
		'<div style="position: relative; opacity: 1;">' .
			'<a href="' . _DIR_RESTREINT . '?exec=acs&amp;onglet=composants&amp;composant=' . $class .
			'&amp;nic=' . $id .
			'"><img src="' . $icon . '" alt="' . $class .
			'" title="' . _T('crayons:editer') . ' ' . $titre . '"	class="bouton_edit_composant" /></a>' .
		'</div>' .
		acs_vue('acsBox', [
			'titre' => $titre,
			'titre2' => $composant->tabs('controleur') . (($composant->nb_widgets > 0) ? '<a class="btn_show_widgets" href="' . _DIR_RESTREINT . '?exec=acs&amp;onglet=composants&amp;composant=' . $class . '&amp;nic=' . $id . '"><img src="' . _DIR_ACS . '/images/composant-24.gif" alt="widgets" height="16px" width="16px" style="float:right" /></a>' : ''),
			'contenu' => $composant->edit([
				'action' => '?action=crayons_composant_store',
				'inputs' =>
					'<input type="hidden" class="crayon-id" name="crayons[]" value="' . $crayon->key . '" />' . "\n" .
					'<input type="hidden" name="name_' . $crayon->key . '" value="' . $crayon->name . '" />' . "\n" .
					'<input type="hidden" name="md5_' . $crayon->key . '" value="' . $crayon->md5 . '" />' . "\n" .
					$ctxhtml .
					'<input type="hidden" name="maj_composant_' . md5($composant->fullname) . '" value="oui" />' .
					'<input type="hidden" name="composant" value="' . $composant->class . '" />' .
					'<input type="hidden" name="nic" value="' . $composant->nic . '" />',
				'buttons' => '<div class="pinceau-boutons">' . crayons_boutons() . '</div>'
			], 'controleur'),
			'class' => 'editeur_composant edit_composant',
			'style' => 'top:0; left:' . $left . 'px',
			'icon' => $composant->icon
		]) .
	'</div>' .
'<script>jQuery(document).ready(
	function() {
		init_controleur_composant();
	}
);</script>';
	$status = null;

	return [$html, $status];
}
