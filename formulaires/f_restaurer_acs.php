<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/acs_restaurer');

/**
 * Formulaire CVT facs_restaurer.html
 */
function formulaires_f_restaurer_acs_charger_dist() {
	return [
		'_dir_dump_acs' => _DIR_DUMP . 'acs/',
		'fichier' => _request('fichier')
	];
}

function formulaires_f_restaurer_acs_verifier_dist() {
	$erreurs = [];
	$fichier = _request('fichier');
	$file = _DIR_DUMP . 'acs/' . basename($fichier); // securite
	if (!is_file($file)) {
		$erreurs['fichier'] = _T('adminacs:err_fichier_absent', ['file' => joli_repertoire($file)]);
	}
	return $erreurs;
}

/**
 * Restaure une sauvegarde ACS
 * @return array
 */
function formulaires_f_restaurer_acs_traiter_dist() {
	$fichier = _request('fichier');
	// On refait des tests secu dans la fonction acs_restaurer(), pas ici.
	return acs_restaurer($fichier);
}
