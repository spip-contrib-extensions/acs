<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Traitement post du formulaire formulaires/f_cfg_acs_set.html
 * @return array
 */
function formulaires_f_cfg_acs_over_traiter_dist() {
		//refuser_traiter_formulaire_ajax(); // a debugguer because pb reset.css
		if (acs_get('ACS_OVER') != _request('ACS_OVER')) {
			acs_set('ACS_OVER', _request('ACS_OVER'));
			acs_recalcul();
			return ['message_ok' => _T('plugin_info_upgrade_ok')];
		}
}
