<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Traitement post du formulaire formulaires/f_cfg_acs_config.html
 * @return array
 */
function formulaires_f_cfg_acs_config_traiter_dist() {
	refuser_traiter_formulaire_ajax();
	if (
		(acs_get('ACS_PREVIEW_BACKGROUND') != _request('ACS_PREVIEW_BACKGROUND')) ||
		(acs_get('ACS_VOIR_PAGES_COMPOSANTS') != _request('ACS_VOIR_PAGES_COMPOSANTS')) ||
		(acs_get('ACS_VOIR_PAGES_PREVIEW') != _request('ACS_VOIR_PAGES_PREVIEW')) ||
		(acs_get('ACS_SPIP_ADMIN_FORM_STYLE') != _request('ACS_SPIP_ADMIN_FORM_STYLE'))
	) {
		acs_set('ACS_PREVIEW_BACKGROUND', _request('ACS_PREVIEW_BACKGROUND'));
		acs_set('ACS_VOIR_PAGES_COMPOSANTS', _request('ACS_VOIR_PAGES_COMPOSANTS'));
		acs_set('ACS_VOIR_PAGES_PREVIEW', _request('ACS_VOIR_PAGES_PREVIEW'));
		acs_set('ACS_SPIP_ADMIN_FORM_STYLE', _request('ACS_SPIP_ADMIN_FORM_STYLE'));
		acs_recalcul();
		return ['message_ok' => _T('plugin_info_upgrade_ok')];
	}
}
