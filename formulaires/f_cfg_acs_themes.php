<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_f_cfg_acs_themes_charger_dist() {
	$posted_themes = [];
	foreach (composants_sets() as $path => $set) {
		if (_request($path . '_theme')) {
			$posted_themes[str_replace('/', '', $path)] = _request($path . '_theme');
		}
	}
	return $posted_themes;
}
/**
 * Traitement post du formulaire formulaires/f_cfg_acs_set.html
 * @return array
 */
function formulaires_f_cfg_acs_themes_traiter_dist() {
	foreach (composants_sets() as $path => $set) {
		if (_request($path . '_theme')) {
			if (_request($path . '_theme') === 'custom') {
				continue;
			}
			$theme_file = _ACS_PLUGIN_SITE_ROOT . $path . '/composants/' . _request($path . '_theme') . '.php';
			if (is_file($theme_file) && is_readable($theme_file)) {
				$backups_dir = _DIR_DUMP . 'acs/';
				if (is_dir($backups_dir) && $d = @opendir($backups_dir)) {
					$list = [];
					$max = 0;
					while ((false !== ($backup = @readdir($backups_dir))) && ($max < 500)) {
						if ($backup[0] != '.') {
							$list[] = date(filemtime($backups_dir . '/' . $backup));
						}
						$max++;
					}
					rsort($list);
				} else {
					return ['message_erreur' => _T('adminacs:themes_change_alert') . '<br><br>' . _T('adminacs:aucune_sauvegarde')];
				}
				$date_bak = new DateTime("@$list[0]");
				$date_maj = new DateTime('@' . acs_get('ACS_MAJ'));
				if ((date_diff($date_bak, $date_maj)->format('%d') > 0) && (_request('force_theme_change') !== 'averti')) {
					return [
						'message_erreur' => _T('adminacs:themes_change_alert') . '<br><br>' . _T('adminacs:sauvegarde_trop_ancienne'),
					];
				} else {
					require_once(_DIR_ACS . 'inc/acs_load_vars.php');
					acs_reset_vars();
					if (acs_load_vars($theme_file) === 'ok') {
						$r .= $theme_file . '<br>';
					}
				}
			}
		}
	}
	if ($r) {
		acs_recalcul();
		return ['message_ok' => _T('plugin_info_upgrade_ok')];
	}
}
