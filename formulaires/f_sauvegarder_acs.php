<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/acs_sauvegarder');

/**
 * Charger formulaire CVT f_sauvegarder_acs.html
 */
function formulaires_f_sauvegarder_acs_charger_dist() {
	$valeurs = [
		'nom_sauvegarde' => acs_nom_sauvegarde(),
		'_dir_dump_acs' => _DIR_DUMP . 'acs/'
	];
	return $valeurs;
}

/**
 * Verifications
 * @return array $erreurs
 */
function formulaires_f_sauvegarder_acs_verifier_dist() {
	$erreurs = [];
	if (!$nom = _request('nom_sauvegarde')) {
		$erreurs['nom_sauvegarde'] = _T('info_obligatoire');
	}
	elseif (!preg_match(',^[\w_][\w_.]*$,', $nom) or basename($nom) !== $nom) {
		$erreurs['nom_sauvegarde'] = _T('dump:erreur_nom_fichier');
	}
	return $erreurs;
}

/**
 * Sauvegarde
 * @return array
 */
function formulaires_f_sauvegarder_acs_traiter_dist() {
	$nom_sauvegarde = _request('nom_sauvegarde');
	return acs_sauvegarder($nom_sauvegarde);
}
