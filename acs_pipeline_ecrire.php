<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Permet d'afficher l'interface d'admin d'ACS dans toutes les langues disponibles pour spip.
//$GLOBALS['meta']['langues_proposees'] = $GLOBALS['meta']['langues_multilingue'];

function acs_header_prive($flux) {
	$r = '<link rel="stylesheet" href="' . _ACS_PLUGIN_SITE_ROOT . 'spip.php?page=acs_composants.css&amp;v=' . acs_get('ACS_MAJ') . '" type="text/css" media="screen" /><script src="' . _ACS_PLUGIN_SITE_ROOT . 'spip.php?page=acs_composants.js&amp;v=' . acs_get('ACS_MAJ') . '"></script>';
	// On ajoute le style privé APRES la feuille de style des sets pour pouvoir
	// overrider ce qui pourrait perturber l'interface privee, comme par exemple la couleur du <body>
	$r .= '<link rel="stylesheet" href="' . generer_url_public('acs_style_prive.css') . '" type="text/css" media="screen" /><script src="' . generer_url_public('javascript/acs_ecrire.js') . '"></script><script src="' . _DIR_ACS . 'inc/picker/picker.js"></script>' . "\r";
	return $flux . $r;
}
