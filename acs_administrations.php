<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
require_once(_DIR_ACS . 'inc/acs_load_vars.php');

function acs_upgrade($from, $to) {
	$maj['create'] = [
		['acs_init']
	];
	$maj['1.0.0'] = [
		['acs_upgrade_cadre_to_v2']
	];
	include_spip('base/upgrade');
	maj_plugin($from, $to, $maj);
}

function acs_vider_tables($nom_meta_base_version) {
	acs_reset_vars();
}

/*
 * Initialisation des variables ACS
 */
function acs_init() {
	require_once(_DIR_ACS . 'inc/acs_version.php');
	// Initialisation des variables avec le thème par défaut
	$theme = find_in_path('composants/def.php');
	$r = acs_load_vars($theme);
	if ($r == 'ok') {
		acs_log('read default values from ' . $theme, _LOG_INFO_IMPORTANTE);
	}	else {
		acs_log('read vars for ACS version ' . acs_version() . $r, _LOG_INFO_IMPORTANTE);
		echo $r;
	}

	// Installation des composants
	$keys = [];
	foreach (array_keys(composants_liste()) as $class) {
		echo ucfirst($class);
		$install_src = find_in_path('composants/' . $class . '/install');
		if (!$install_src) {
			echo '<br>';
			continue;
		}
		// installation des images du composant
		if (is_readable($install_src . '/IMG')) {
			$result = copy_dir($install_src . '/IMG/', '../' . $GLOBALS['ACS_CHEMIN'] . '/');
			acs_log($class . ' images installed in ' . $GLOBALS['ACS_CHEMIN'] . '/ from ' . $install_src, _LOG_INFO_IMPORTANTE);
			echo ', <a class="acsLabel" title="' . implode("\n", $result['files']) . '">images</a>' . ($result['err'] ? '<div class="alert">' . nl2br($result['err']) . '</div>' : '');
		}
		// lecture des mots-clefs du composant
		if (is_readable($install_src . '/keywords/' . $class . '_keywords_' . $GLOBALS['lang'] . '.php')) {
			$keyfile = $install_src . '/keywords/' . $class . '_keywords_' . $GLOBALS['lang'] . '.php';
		} else {
			$keyfile = $install_src . '/keywords/' . $class . '_keywords_fr.php';
		}
		// Si on ne trouve pas de mot-clés, on passe au composant suivant
		if (!is_readable($keyfile)) {
			if (is_readable($install_src . '/keywords/')) {
				acs_log('échec de lecture du fichier des mots-clefs du composant ' . $class . ' : ' . $keyfile, _LOG_INFO_IMPORTANTE);
			}
			echo '<br>';
			continue;
		}
		// On lit les mots-clefs du composant
		require_once($keyfile);
		if (!is_array($keywords)) {
			acs_log('échec de lecture des mots-clefs du composant ' . $class .  ' depuis ' . $keyfile, _LOG_ERREUR);
			echo '<br>';
			continue;
		}
		$keys = array_merge_recursive($keys, $keywords);
		echo ', ' . strtolower(_T('mots:titre_groupes_mots')) . ' ' . implode(', ', array_keys($keywords)) . '<br>';
	}

	// Installation des mots-clefs des composants
	// On teste l'existence des deux tables des mots-clefs
	$trouver_table = charger_fonction('trouver_table', 'base');
	$table_desc_groupes_mots = $trouver_table('groupes_mots');
	$table_desc_mots = $trouver_table('mots');
	if (is_array($table_desc_groupes_mots) && (is_array($table_desc_mots))) {
		// On recupere les noms des tables
		$table_groupes_mots = $table_desc_groupes_mots['table'];
		$table_mots = $table_desc_mots['table'];

		// On construit la requête SQL
		foreach ($keys as $grtitre => $kg) {
			$maj = [
				'titre' => $grtitre,
				'descriptif' => is_array($kg['descriptif']) ? implode("\n", $kg['descriptif']) : $kg['descriptif'],
				'texte' => is_array($kg['texte']) ? implode("\n", $kg['texte']) : $kg['texte'],
				'unseul' => 'oui',
				'obligatoire' => 'non',
				'tables_liees' => is_array($kg['tables_liees']) ? implode(',', $kg['tables_liees']) : $kg['tables_liees'],
				'minirezo' => 'oui',
				'comite' => 'non',
				'forum' => 'non',
			];
			// On recupere la clef primaire
			$id_groupe = sql_getfetsel('id_groupe', $table_groupes_mots, 'titre="' . $grtitre . '"');
			if ($id_groupe) {
				if (!sql_updateq($table_groupes_mots, $maj, 'id_groupe="' . $id_groupe . '"')) {
					acs_log('acs_init(): unable to update keywords group "' . $grtitre . '": ' . sql_error(), _LOG_ERREUR);
				}
			} else {
				$id_groupe = sql_insertq($table_groupes_mots, $maj);
				// a ce stade, si l'on a pas d'id_groupe, c'est qu'il n'existait pas ET n'a pas pu etre cree
				if (!$id_groupe) {
					acs_log('acs_init(): unable to create keywords group "' . $grtitre . '": ' . sql_error(), _LOG_ERREUR);
					continue;
				}
			}
			foreach ($kg['mots'] as $mottitre => $mots) {
				if (sql_getfetsel('id_mot', $table_mots, 'titre="' . $mottitre . '" AND id_groupe="' . $id_groupe . '"')) {
					continue;
				}
				if (sql_insertq($table_mots, ['titre' => $mottitre, 'descriptif' => $mots['descriptif'], 'texte' => $mots['texte'], 'id_groupe' => $id_groupe, 'type' => $grtitre])) {
					acs_log('acs_init(): keyword "' . $mottitre . '" created in group "' . $grtitre . '"', _LOG_DEBUG);
				} else {
					acs_log('acs_init(): unable to create keyword "' . $mottitre . '" in group "' . $grtitre . '": ' . sql_error(), _LOG_ERREUR);
					continue;
				}
			}
		}
	}
	acs_set('ACS_VERSION', acs_version()); // Ecrire la meta signalant la version installee
}

function acs_upgrade_cadre_to_v2() {
	$upf = 'upgrade_cadre_to_v2';
	$url = include_spip('upgrade/' . $upf);
	if ($url) {
		$upr = $upf();
	} else {
		acs_log('function ' . $upf . '() not found in set.', _LOG_DEBUG);
	}
}

// Copie recursive d'un dossier
function copy_dir($dir2copy, $dir_paste, $files = []) {
	$err = '';
	// On verifie si $dir2copy existe et est un dossier
	if ($dir2copy && is_dir($dir2copy)) {
		// Si le dossier destination n'existe pas, on le cree
		include_spip('inc/acs_mkdir_recursive');
		if (!mkdir_recursive($dir_paste)) {
			return 'Unable to create ' . $dir_paste;
		}
		if ($dh = opendir($dir2copy)) {
			// On liste les dossiers et fichiers de $dir2copy
			while (($file = readdir($dh)) !== false) {
				if ($file[0] == '.') {
					continue;
				}
				// S'il s'agit d'un dossier accessible en écriture on relance la fonction récursive
				if (is_dir($dir2copy . $file)) {
					if (is_writable($dir_paste)) {
						$r = copy_dir($dir2copy . $file . '/', $dir_paste . $file . '/', $files);
						$err .= $r['err'];
						$files = $r['files'];
					} else {
						$err .= 'Unable to create ' . $file . ' in ' . $dir_paste . "\n";
					}
				}
				// S'il sagit d'un fichier, on le copie
				else {
					if (copy($dir2copy . $file, $dir_paste . $file)) {
						$files[] = $file;
					} else {
						$err .= 'Unable to copy ' . $dir2copy . $file . ' to ' . $dir_paste . $file . "\n";
					}
				}
			}
		// On ferme $dir2copy
		closedir($dh);
		}
	}
	return ['files' => $files, 'err' => $err];
}
