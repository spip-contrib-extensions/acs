<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Implémentation du pipeline insert_head pour le plugin ACS.
 * Recherche	a la racine du set de composants les fichiers de la forme
 * "set_de_composants.css.html" et "javascript/set_de_compsoants.js.html", et
 * insere leur lien dans le header de la page web s'il existe.
 *
 * insert_head pipeline for ACS plugin.
 */
function acs_insert_head($flux) {
	$r = '<link rel="stylesheet" href="' . generer_url_public('acs_composants.css', 'v=' . acs_get('ACS_MAJ')) . '" type="text/css" media="screen"><script src="' . generer_url_public('acs_composants.js', 'v=' . acs_get('ACS_MAJ')) . '"></script>';

	// On ajoute la CSS et les javascripts des administrateurs ACS
	if (autoriser('acs', 'pinceaux')) {
		$r .= '<link rel="stylesheet" href="' . direction_css(generer_url_public('acs_style_prive.css')) . '" type="text/css" media="screen">';
		$r .= '<script src="' . urldecode(generer_url_public('javascript/acs_controleur_composant.js')) . '"></script>';
	}
	return $flux . $r;
}
