<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce wrapper permet d'être dans le bon répertoire pour appeller filepicker.php
 * et d'accéder ainsi aux variables globales et à l'API SPIP
 */

function action_filepickerwrapper() {
	include(_DIR_ACS . 'inc/picker/filepicker.php');
}
