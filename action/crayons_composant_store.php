<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Crayon pour un composant - Sauvegarde
 * Crayon for one component - Store changes
 */
function action_crayons_composant_store_dist() {
	include_spip('inc/crayons');

	acs_log('by ' . $GLOBALS['auteur_session']['id_auteur'], _LOG_INFO);
	lang_select($GLOBALS['auteur_session']['lang']);
	header('Content-Type: text/html; charset=' . $GLOBALS['meta']['charset']);

	// anti-injection de pagination après mise à jour du composant.
	if (isset($_GET['var_ajax'])) {
		$r = _T('avis_operation_impossible') . '<br>' . _T('admin_recalculer'); // Il faut recalculer la page !
		echo '<span class="alert alert_box">' . $r . '</span>';
		exit;
	}

	// Dernière sécurité :Accès réservé aux admins ACS
	// Last security: access restricted to ACS admins
	if (!autoriser('acs', 'crayons_composant_store')) {
		echo crayons_var2js(['$erreur' => _T('avis_operation_impossible')]);
		exit;
	}

	$wid = $_POST['crayons'][0];
	$c = 'composants/' . $_POST['composant'] . '/' . $_POST['composant'];
	// MàJ du composant - Update component : l'instanciation d'un objet composant fait la mise a jour
	require_once(_DIR_ACS . 'inc/composant/classAdminComposant.php');
	new AdminComposant($_POST['composant'], $_POST['nic']);
	// Retourne la vue - Return vue
	$contexte = ['var_mode' => 'recalcul'];
	$contextes_ids = ['id_article', 'id_rubrique', 'id_mot', 'id_groupe', 'recherche', 'page'];
	foreach ($contextes_ids as $cid) {
		if (isset($_POST[$cid])) {
			$contexte = array_merge([$cid => 'context_' . $_POST[$cid]], $contexte);
		}
	}
	// langue traitee a part pour pouvoir donner une valeur par defaut
	if (isset($_POST['lang'])) {
		$lang = $_POST['lang'];
	}
	else {
		$lang = $GLOBALS['spip_lang'];
	}
	$contexte = array_merge(['lang' => $lang], $contexte);

	$return[$wid] = vues_dist('composant', $c, $_POST['nic'], $contexte);
	$return['$erreur'] = '';
	echo crayons_var2js($return);
	exit;
}

function vues_dist($type, $modele, $id, $content) {
	if (find_in_path(($fond = 'vues/composant') . '.html')) {
		$contexte = [
			'nic' => $id,
			'c' => $modele,
			'lang' => $GLOBALS['spip_lang']
		];
		$contexte = array_merge($contexte, $content);
		include_spip('public/assembler');
		return recuperer_fond($fond, $contexte);
	}
	// vue par defaut
	else {
		// pour ce qui a une {lang_select} par defaut dans la boucle,
		// la regler histoire d'avoir la bonne typo dans le propre()
		if (colonne_table($type, 'lang')) {
			lang_select($a = valeur_colonne_table($type, 'lang', $id));
		} else {
			lang_select($a = $GLOBALS['meta']['langue_site']);
		}
	return 'err 404';
	}
}
