<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Crayon pour une variable ACS - Sauvegarde
 * Crayon for one ACS variable - Store changes
*/
function action_crayons_var_store_dist() {
	include_spip('inc/crayons');
	// Inclusion de l'action crayons_store du plugin crayons, comme librairie
	include_spip('action/crayons_store');
	// Inclusion de la definition de la vue d'une variable
	require_once(_DIR_ACS . 'inc/acs_vars.php');

	lang_select($GLOBALS['auteur_session']['lang']);
	header('Content-Type: text/html; charset=' . $GLOBALS['meta']['charset']);

	// anti-injection de pagination après mise à jour du composant.
	if (isset($_GET['var_ajax'])) {
			$r = _T('avis_operation_impossible') . '<br>' . _T('admin_recalculer'); // Il faut recalculer la page !
			echo '<span class="alert alert_box">' . $r . '</span>';
			exit;
	}

	// Dernière sécurité :Accès réservé aux admins ACS
	// Last security: access restricted to ACS admins
	if (!autoriser('acs', 'crayons_var_store')) {
		echo crayons_var2js(['$erreur' => _T('avis_operation_impossible')]);
		exit;
	}

	$wid = $_POST['crayons'][0];
	$name = $_POST['name_' . $wid];
	$name = explode('-', $name);
	$nic = $name[2];
	$var = $_POST['fields_' . $wid];
	$v = explode('_', $var);
	$c = $v[0]; // composant
	$v = $v[1]; // variable
	$var = 'acs' . ucfirst($c) . ($nic ? $nic : '') . $v;
	$oldval = $_POST['oldval'];
	// est-ce que la variable a ete modifiée entre-temps ?
	if ($oldval != acs_get($var)) {
		echo crayons_var2js(['$erreur' => _U('crayons:modifie_par_ailleurs')]);
		exit;
	}
	$type = $_POST['type'];
	switch ($type) {
		case 'bord':
			$color = $_POST[$var . 'Color'];
			$style = $_POST[$var . 'Style'];
			$width = $_POST[$var . 'Width'];
			$newval = serialize(['Color' => $color, 'Style' => $style, 'Width' => $width]);
			break;
		case 'key':
			$group = $_POST[$var . 'Group'];
			$key = $_POST[$var . 'Key'];
			$newval = serialize(['Group' => $group, 'Key' => $key]);
			break;
		default:
			$newval = $_POST[$var];
	}
	acs_set($var, $newval);
	acs_recalcul(); // forcera un recalcul
	acs_log('ACS action/crayons_var_store ' . $var . ' => ' . $newval, _LOG_INFO);
	// Retourne la vue - Return vue
	$return['$erreur'] = null;
	$return[$wid] = affiche_variable($type, $var);
	echo crayons_var2js($return);
	exit;
}
