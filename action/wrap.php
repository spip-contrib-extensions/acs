<?php

/**
 * Ce wrapper permet d'être dans le bon répertoire
 * et d'accéder aux variables globales
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_wrap() {
	if (isset($_GET['cadre'])) {
		include($_GET['cadre']);
	}
}
