
		Assistant de Configuration du Site

Version : 4.3.6

Documentation : http:// acs.geomaticien.org
Licence: cf LICENCES.txt
________________________________________________________________________________
Dernière mise à jour de ce document le: 01-10-2024
Par: Daniel FAIVRE
________________________________________________________________________________

ACS permet de créer des sites SPIP à base de composants paramétrables.

Le webmestre peut modifier couleurs, styles, images de fonds, ...	
et plus généralement n'importe quel paramètre sans éditer de fichiers pour adapter 
rapidement squelettes et feuilles de style à sa charte graphique.

ACS intègre un "set ACS" (jeu de squelettes SPIP) nommé "Cat",
entièrement personnalisable par interface web, multilingue, et extensible.
________________________________________________________________________________
Utilisation de composants ACS-Cat dans d'autres squelettes SPIP que ceux de Cat :
- indiquez dans la configuration d'ACS votre dossier de squelettes.
Ceux-ci seront utilisés sur le site public à la place de ceux du modèle cat d'ACS.
Les composants ACS y deviennent ainsi insérables sous la forme d'inclusions SPIP
ou du modèle composant (issu de Cat). Exemples :

<INCLURE{fond=composants/un_composant/un_sous_composant}{parametre1=truc}>
[(#MODELE{composant}{c=un_composant/un_sous_composant}{parametre1=truc})]

Tout composant ACS peut également être inséré et paramétré directement dans un élément éditable
de SPIP sous forme de modèle SPIP. Exemple :

<composant|c=uncomposant/uncomposant|parametre1=truc|parametre2=machin>
________________________________________________________________________________
Pour développeur de squelettes :

Chaque composant ACS du modèle actif peut être intégré dans un jeu de squelettes
"d'override" en surcouche(s) d'ACS et qui peut posséder 
ses propres composants personnalisés, dans un dossier "composants".
Tout fichier composants/manoisette/manoisette.html accessible dans les chemins où SPIP va chercher
ses éléments de squelettes est un composant.
ACS affiche ces chemins dans l'ordre des surcharges (override) dans Configuration/ACS.

