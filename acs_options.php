<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * \file acs_options.php
 * \~english
 * First file of the plugin loaded.
 * Debug options and plugin load.
 * \~french
 * Premier fichier du plugin chargé, avant même autoriser.php de la dist, mais après les fichiers de langues.
 * Options de debug et chargement du plugin.
 */

// Uncomment for debugs :
//define('_DEBUG_CRAYONS', true); // Debug plugin crayons
//define('_DEBUG_AUTORISER', true); // debug autorisations SPIP
define('_ACS_LOG', _LOG_HS); // niveau de log ACS

/*
 * Constantes de niveau de log (ACS et SPIP)
 * 0 (_LOG_HS)
 * 1 (_LOG_ALERTE_ROUGE)
 * 2 (_LOG_CRITIQUE)
 * 3 (_LOG_ERREUR)
 * 4 (_LOG_AVERTISSEMENT)
 * 5 (_LOG_INFO_IMPORTANTE)
 * 6 (_LOG_INFO)
 * 7 (_LOG_DEBUG)
 */

/*__________________________________________________________________

   Ne PAS modifier ce qui suit - Do NOT modify anything after this
	__________________________________________________________________
*/

// Contourne provisoirement le bug de plugins-dist/compresseur
// cf. https://git.spip.net/spip/compresseur/-/issues
define('_INTERDIRE_COMPACTE_HEAD_ECRIRE', true);

//define('_LOG_FILTRE_GRAVITE', _ACS_LOG); // niveau de log SPIP au niveau d'ACS

// Empêche SPIP d'indiquer sa version dans le header http
$GLOBALS['spip_header_silencieux'] = 1;

// Retourne une variable d'un plugin actif
function acs_get_from_active_plugin($plugin, $variable = false) {
	$meta_plugin = unserialize($GLOBALS['meta']['plugin']);
	$plugin = $meta_plugin[$plugin];

	if (!$variable) {
		return isset($plugin['dir']);
	}
	if (isset($plugin[$variable])) {
		return $plugin[$variable];
	}
	return false;
}

$dir_plugin_acs = acs_get_from_active_plugin('ACS', 'dir');
// Chemin valable espace public aussi, pas comme _DIR_PLUGIN_ACS, qui est à proscrire
define('_DIR_ACS', _DIR_PLUGINS . $dir_plugin_acs . '/');

// Dossier des paramètres et images utilisateur
// User images and parameters
// compatible mutualisation (_DIR_SITE defini)
if (defined('_DIR_SITE')) {
	// Racine des dossiers du site d'installation du plugin ACS
	define('_ACS_PLUGIN_SITE_ROOT', _DIR_RACINE); // todo : permettre install du plugin ACS dans un site mutualisé
	// Racine des dossiers DU SITE :
	define('_ACS_DIR_SITE_ROOT', _DIR_SITE);
	$dir_site_absolu = _DIR_RACINE ? substr(_DIR_SITE, 3) : _DIR_SITE;
}
else {
	define('_ACS_PLUGIN_SITE_ROOT', _DIR_RACINE);
	define('_ACS_DIR_SITE_ROOT', _DIR_RACINE);
	$dir_site_absolu = '';
}

// Fonctions ACS
require_once(_DIR_ACS . 'inc/acs_lib.php');
require_once(_DIR_ACS . 'inc/composant/composants_lire.php');

$paths = ((acs_get('ACS_OVER') ? acs_get('ACS_OVER') . ':' : '') . implode(':', array_keys(composants_sets())));
$dossiers_squelettes_avant_override = explode(':', $GLOBALS['dossier_squelettes']);
$paths = array_unique(explode(':', $paths)); // Doublons possibles si composants dans les dossiers d'override

foreach ($paths as $dir) {
	@include(_ACS_PLUGIN_SITE_ROOT . $dir . '/mes_options.php');
	@include(_ACS_PLUGIN_SITE_ROOT . $dir . '/mes_fonctions.php');
	if (in_array(_ACS_PLUGIN_SITE_ROOT . $dir, $dossiers_squelettes_avant_override)) {
		continue;
	}
	if (isset($GLOBALS['dossier_squelettes']) && $GLOBALS['dossier_squelettes']) {
		$GLOBALS['dossier_squelettes'] .= ':';
	}
	$GLOBALS['dossier_squelettes'] .= $dir;
}

// Set ACS par defaut - Default ACS set
acs_set('ACS_SET', 'cat');
acs_set('ACS_SET_DIR', 'plugins/' . $dir_plugin_acs . '/' . 'sets/' . acs_get('ACS_SET'));

// Dossier des images personnalisées du set ACS actif - Active ACS set customized images directory
$GLOBALS['ACS_CHEMIN'] = $dir_site_absolu . _NOM_PERMANENTS_ACCESSIBLES . '_acs/' . acs_get('ACS_SET');

// dispatch public / privé
if ((_DIR_RESTREINT != '') && isset($_POST['action']) && ($_POST['action'] != 'poster_forum_prive')) {
	// Sauts de ligne
	$GLOBALS['spip_pipeline']['pre_propre'] .= '|post_autobr';
}

// Charge la liste des classes de composants dans une globale (gain de temps, on force le calcul de composants_lire() très tôt)
if (!isset($GLOBALS['ACS_COMPOSANTS_CLASSES'])) {
	$GLOBALS['ACS_COMPOSANTS_CLASSES'] = serialize(array_keys(composants_lire()));
}

// On ajoute les balises de chaque composant actif - We add tags for every active component
foreach (composants_balises() as $bf) {
	include(_ACS_PLUGIN_SITE_ROOT . $bf); // Les erreurs ne doivent JAMAIS être masquées, ici
}

// Balises et traductions, avec fonctions SPIP requises partout
require_once(_DIR_ACS . 'inc/sp/acs_spip.php');
