<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!autoriser('acs')) {
	sinon_interdire_acces();
}

/**
 * Retourne les pages du squelette qui utilisent le composant $c
 * Fonction appellée en mode Ajax et mise en cache/acs
 */


if (!is_callable('ajax_retour')) {
	include_spip('inc/actions');
}
include_spip('inc/acs_cache');
include_spip('inc/composant/composant_infos');

function exec_composant_get_infos() {
	$c = _request('c');
	$nic = _request('nic');
	$var_mode = _request('var_mode');

	$err = '<span class="alert" title="' . _T('adminacs:err_cache') . '">*</span>';

	$r = acs_cache(
		'composant_infos',
		'c_' . $c . $nic . '_infos',
		['c' => $c,'nic' => $nic],
		($var_mode == 'recalcul')
	);
	if (!is_array($r) || count($r) < 2) {
		ajax_retour($err);
	}
	if ($r[1] != 'err') {
		$err = '';
	}
	ajax_retour($r[0] . $err);
}
