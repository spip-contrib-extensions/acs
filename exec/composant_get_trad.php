<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!autoriser('acs')) {
	sinon_interdire_acces();
}
include_spip('inc/actions');
/**
 * Retourne une traduction du composant $c
 */
function exec_composant_get_trad() {
	$c = _request('c');
	$trcmp = _request('trcmp');
	$context = _request('context');

	include_spip('inc/composant/composant_traduction');
	$r =	composant_traduction($c, $trcmp, $context);

	ajax_retour($r);
}
