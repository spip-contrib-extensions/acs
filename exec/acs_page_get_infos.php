<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!autoriser('acs')) {
	sinon_interdire_acces();
}

function exec_acs_page_get_infos() {
	require_once(_DIR_ACS . 'inc/acs_page_get_infos.php');
	ajax_retour(
		acs_page_get_infos(
			_request('pg'),
			_request('mode'),
			_request('detail'),
			_request('var_mode')
		)
	);
}
