<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!autoriser('acs')) {
	sinon_interdire_acces();
}
include_spip('inc/actions');
/**
 * Retourne les traductions du compoant $c
 */
function exec_composant_get_traductions() {
	$c = _request('c');

	include_spip('inc/composant/composant_traductions');
	$r =	composant_traductions($c);

	ajax_retour($r);
}
