<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!autoriser('acs')) {
	sinon_interdire_acces();
}

function exec_acs_var_vals() {
	require_once(_DIR_ACS . 'inc/acs_var_vals.php');
	return acs_var_vals(_request('var'));
}
