<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function exec_acs() {
	global $connect_statut, $connect_toutes_rubriques, $options, $spip_lang_left, $spip_lang_right, $spip_display;

	if (!autoriser('acs')) {
		sinon_interdire_acces();
	}
	include_spip('inc/config');
	include_spip('inc/meta');
	include_spip('inc/presentation');
	require_once(_DIR_ACS . 'inc/acs_vue.php');

	if (_request('onglet')) {
		$onglet = _request('onglet');
	}
	else {
		$onglet = 'pages';
	}

	// Affichage
	// (spip-hack: le debut_page doit se trouver avant la création d'un objet composant pour disposer des bons include spip⁾)
	$commencer_page = charger_fonction('commencer_page', 'inc');
	echo $commencer_page(_T('adminacs:acs_description'), 'configuration', 'acs');
	switch ($onglet) {
		case 'pages':
			require_once(_DIR_ACS . 'inc/acs_pages.php');
			if (_request('pg')) {
				$pg = _request('pg');
			}
			else {
				$pg = 'sommaire';
			}
			$col1 = acs_vue('acsBox', [
				'titre' => '<label class="acsLabel" title="' . _T('adminacs:onglet_pages_help') . '">' . _T('adminacs:pages') . '</label>',
				'titre2' => '',
				'contenu' => liste_pages_du_site('pages'),
				'class' => 'acs_box_pages',
				'style' => '',
				'icon' => _DIR_ACS . 'images/pages-24.gif'
			]);
			$col2 = acs_pages($pg);
			$col3 = acs_pages_droite($pg);
			break;

		case 'vars':
			require_once(_DIR_ACS . 'inc/acs_vars.php');
			$col1 = acs_vars_gauche();
			$col2 = acs_vars(_request('vf'));
			$col3 = '';
			break;

		case 'composants':
			require_once(_DIR_ACS . 'inc/acs_composants.php');
			require_once(_DIR_ACS . 'inc/composant/classAdminComposant.php');
			require_once(_DIR_ACS . 'inc/acs_widgets.php');

			// Crée l'objet composant - Create current component object
			$cc = _request('composant') ? _request('composant') : 'fond';
			$nic = _request('nic', $_POST) ? _request('nic', $_POST) : _request('nic');
			$c = $cc . $nic;
			$$c = new AdminComposant($cc, $nic, (_ACS_LOG >= _LOG_DEBUG));

			// Crée l'interface d'administration du composant
			$col1 = acs_vue('acsBox', [
				'titre' => _T('adminacs:composant'),
				'titre2' => '<img src="' . _DIR_ACS . '/images/info.png" alt="&#9432;" />',
				'contenu' => $$c->infos(),
				'class' => '',
				'style' => '',
				'icon' => $$c->icon
			]);
			$col2 = composants($$c);
			$col3 = liste_widgets($$c);
			break;
	}
	echo debut_onglet();
	echo onglet(
		_T('adminacs:pages'),
		generer_url_ecrire('acs', 'onglet=pages'),
		$onglet,
		'pages',
		_DIR_ACS . '/images/pages-24.gif'
	);
	echo onglet(
		_T('adminacs:composants'),
		generer_url_ecrire('acs', 'onglet=composants'),
		$onglet,
		'composants',
		_DIR_ACS . '/images/composant-24.gif'
	);
	echo onglet(
		_T('adminacs:variables'),
		generer_url_ecrire('acs', 'onglet=vars'),
		$onglet,
		'vars',
		_DIR_ACS . '/images/vars-24.gif'
	);
	echo fin_onglet();
	echo acs_vue('acs3colonnes', [
			'col1' => $col1,
			'col2' => $col2,
			'col3' => $col3,
		]);
	echo fin_page();
}

// retourne une boîte alerte de SPIP
function acs_alert_box($msg) {
	return debut_boite_alerte() . $msg . fin_boite_alerte();
}
