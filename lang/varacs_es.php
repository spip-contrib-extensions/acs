<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/varacs-acs?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'align' => 'Alineación',

	// B
	'bordcolor' => 'Borde',
	'bordlargeur' => 'ancho del borde',
	'bordrond' => 'Redondeo de bordes',
	'bordstyle' => 'estilo de borde',
	'bottom' => 'bajo',

	// C
	'center' => 'centro',

	// D
	'dashed' => 'guiones',
	'dotted' => 'punteado',
	'double' => 'líneas sólidas dobles',

	// F
	'fond' => ' Fondo',
	'font' => 'Tipografía(s)',
	'fontfamily' => 'Familia tipográfica',
	'fontsize' => 'Talla',

	// G
	'groove' => 'grabado (reverso del ridge)',

	// H
	'horizontal' => 'horizontal',

	// I
	'inset' => 'incrustado en la página (inverso del outset)',

	// K
	'key' => 'Palabra clave', # RELIRE

	// L
	'left' => 'izquierda',
	'link' => 'Hiperenlace', # RELIRE
	'linkhover' => 'Al pasar el mouse',

	// M
	'margin' => 'Margen',
	'middle' => 'medio',

	// N
	'nb' => 'Número',
	'nom' => ' Nombre',
	'non' => 'No',
	'none' => 'sin borde, equivalente a border-width:0',

	// O
	'oui' => 'Si',
	'outset' => 'extruido de la página (inverso del inset)',

	// P
	'padding' => 'Margen interior',
	'parent' => 'valor predeterminado',

	// R
	'ridge' => 'se sale de la página (inverso del groove)',
	'right' => 'derecho',

	// S
	'shadow' => 'Sombra',
	'shadowblur' => 'Borroso', # RELIRE
	'shadowsize' => 'Talla',
	'solid' => 'línea completa',

	// T
	'text' => 'Texto',
	'titrefond' => 'Fondo del título',
	'top' => 'alto',

	// U
	'use' => 'Usar',

	// V
	'valign' => 'Alineamiento vertical',
	'vertical' => 'vertical'
);
