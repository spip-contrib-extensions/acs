<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/varacs-acs?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'align' => 'Alignment',

	// B
	'bordcolor' => 'Border',
	'bordlargeur' => 'border width',
	'bordrond' => 'Rounded corner',
	'bordstyle' => 'border style',
	'bottom' => 'bottom',

	// C
	'center' => 'center',

	// D
	'dashed' => 'dashed',
	'dotted' => 'dotted',
	'double' => 'double',

	// F
	'fond' => 'Background',
	'font' => 'Font(s)',
	'fontfamily' => 'Font family',
	'fontsize' => 'Size',

	// G
	'groove' => 'groove',

	// H
	'horizontal' => 'horizontal',

	// I
	'inset' => 'inset',

	// K
	'key' => 'Keyword',

	// L
	'left' => 'left',
	'link' => 'Link',
	'linkhover' => 'Link hover',

	// M
	'margin' => 'Margin',
	'middle' => 'middle',

	// N
	'nb' => 'Number',
	'nom' => ' Name',
	'non' => 'No',
	'none' => 'no border. Equivalent to border-width: 0',

	// O
	'oui' => 'Yes',
	'outset' => 'outset',

	// P
	'padding' => 'Padding',
	'parent' => 'default value',

	// R
	'ridge' => 'ridge',
	'right' => 'right',

	// S
	'shadow' => 'Shadow',
	'shadowblur' => 'Blur',
	'shadowsize' => 'Size',
	'solid' => 'solid',

	// T
	'text' => ' Text',
	'titrefond' => 'Title background',
	'top' => 'top',

	// U
	'use' => 'Use',

	// V
	'valign' => 'Vertical alignment',
	'vertical' => 'vertical'
);
