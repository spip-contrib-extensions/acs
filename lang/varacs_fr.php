<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/acs.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'align' => 'Alignement',

	// B
	'bordcolor' => 'Bordure',
	'bordlargeur' => 'largeur de bordure',
	'bordrond' => 'Arrondi de bordure',
	'bordstyle' => 'style de bordure',
	'bottom' => 'bas',

	// C
	'center' => 'centré',

	// D
	'dashed' => 'tirets',
	'dotted' => 'pointillés',
	'double' => 'double traits pleins',

	// F
	'fond' => ' Fond',
	'font' => 'Fonte(s)',
	'fontfamily' => 'Famille de fonte',
	'fontsize' => 'Taille',

	// G
	'groove' => 'gravé (inverse de ridge)',

	// H
	'horizontal' => 'horizontal',

	// I
	'inset' => 'incrusté dans la page (inverse de outset)',

	// K
	'key' => 'Mot-clef',

	// L
	'left' => 'gauche',
	'link' => 'Lien',
	'linkhover' => 'Au survol',

	// M
	'margin' => 'Marge',
	'middle' => 'milieu',

	// N
	'nb' => 'Nombre',
	'nom' => ' Nom',
	'non' => 'Non',
	'none' => 'pas de bordure, équivaut à border-width:0',

	// O
	'oui' => 'Oui',
	'outset' => 'extrudé de la page (inverse de inset)',

	// P
	'padding' => 'Marge intérieure',
	'parent' => 'valeur par défaut',

	// R
	'ridge' => 'sort de la page (inverse de groove)',
	'right' => 'droit',

	// S
	'shadow' => 'Ombre',
	'shadowblur' => 'Flou',
	'shadowsize' => 'Taille',
	'solid' => 'trait plein',

	// T
	'text' => 'Texte',
	'titrefond' => 'Fond titre',
	'top' => 'haut',

	// U
	'use' => 'Utiliser',

	// V
	'valign' => 'Alignement vertical',
	'vertical' => 'vertical'
);
