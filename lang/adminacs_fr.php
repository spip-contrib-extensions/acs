<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/acs.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'acs' => 'ACS',
	'acs_description' => 'Conception du Site',
	'acs_help' => 'Les composants peuvent être utilisés dans les squelettes sous forme d’inclusions SPIP ou du modèle <b>composant</b>. Exemples :<br><br>
<b>&lt;INCLURE{fond=composants/un_composant/un_sous_composant}{parametre1=truc}&gt;<br>
[(#MODELE{composant}{c=un_composant/un_sous_composant}{parametre1=truc})]</b><br><br>

Tout composant ACS peut également être inséré et paramétré directement dans un élément éditable
de SPIP sous forme de modèle SPIP :<br><br>

<b>&lt;composant|c=uncomposant/uncomposant|parametre1=truc|parametre2=machin&gt;</b><br><br>
Tout fichier <b>composants/manoisette/manoisette.html</b> accessible dans les chemins où SPIP cherche ses squelettes est un composant.',
	'acs_info' => 'Tout composant actif peut être intégré dans un jeu de squelettes
"d’override" personnalisé et qui peut posséder
ses propres composants.',
	'acs_info_title' => 'Personnalisation',
	'acsdernieremodif' => 'Mis à jour le',
	'adm' => 'Administration',
	'admins' => 'Administrateurs',
	'afterUpdate_not_callable' => 'Méthode afterUpdate introuvable',
	'aucune_sauvegarde' => 'Aucune sauvegarde',

	// B
	'boucle' => 'Boucle',
	'bug_report' => 'Signaler un bug',

	// C
	'choix_couleur' => 'Choix de couleur',
	'choix_image' => 'Choisir une image',
	'composant' => 'Composant',
	'composant_non_utilise' => 'Composant non utilisé',
	'composants' => 'Composants',
	'creer_composant' => 'Créer une nouvelle instance de ce composant',

	// D
	'del_composant' => 'Effacer cette instance de composant',
	'del_composant_confirm' => 'Voulez-vous vraiment effacer DEFINITIVEMENT l’instance @nic@ du composant @c@ ?',
	'dev_infos' => 'Infos développeur',
	'documentation' => 'Documentation',

	// E
	'echec_afterUpdate' => 'Echec afterUpdate',
	'ecrire' => 'Ecrire',
	'effacer_image' => 'Effacer DEFINITIVEMENT "@image@" du serveur ???',
	'effacer_variables_fantomes' => 'Effacer les variables fantômes',
	'err_aucun_composant' => 'Aucun composant actif pour ',
	'err_cache' => 'Impossible de lire ou d’écrire dans le cache ACS',
	'err_del_file' => 'Impossible d’effacer le fichier',
	'err_fichier_absent' => 'Fichier @file@ introuvable',
	'err_fichier_ecrire' => 'Impossible d’écrire dans &quot;@file@&quot;',
	'error_no_set' => 'Aucun set',

	// F
	'formulaire' => 'Formulaire',
	'formulaires' => 'Formulaires',

	// G
	'groupes' => 'Groupes',

	// I
	'impossible_ouvrir_dossier' => 'Impossible d’ouvrir le dossier',

	// M
	'modele' => 'Modèle',
	'modeles' => 'Modèles',

	// N
	'not_found' => 'introuvable',

	// O
	'onglet_pages_help' => 'Les pages soulignées sont lues dans le dossier de squelettes en <span style=&quot;color: darkgreen; font-weight: normal; font-style: normal; text-decoration: underline&quot;>surcharge</span> d’ACS, les pages du <span style=&quot;color: darkgreen; font-weight: bold; font-style: normal; text-decoration: none&quot;>modèle ACS</span> actif sont en gras, celles des <span style=&quot;color: darkgreen; font-weight: normal; font-style: italic; text-decoration: none&quot;>plugins</span> en italique, et celles de la <span style=&quot;color: darkgreen; font-weight: normal; font-style: normal; text-decoration: none&quot;>distribution spip</span> sans décoration.

Ce sont des <i>&quot;noisettes&quot;</i>, c’est à dire des squelettes de blocs fonctionnels personnalisables à inclure dans les pages du site.
',
	'onglet_pages_info' => 'ACS ajoute à spip des modèles de pages personnalisables par assemblage de composants eux-même personnalisables.',
	'overriden_by' => ', surchargé par les squelettes de <u>@over@</u>',

	// P
	'page' => 'Page',
	'page_rien_a_signaler' => 'Ni variables, ni boucles, ni inclusions.',
	'pages' => 'Pages',
	'pg_help' => 'Le <b>schéma</b> de la page présente les <span class=&quot;col_BOUCLE&quot;>boucles spip</span> et les inclusions. Un clic sur le petit triangle noir affiche un <b>schéma détaillé</b>.
<b>Source</b> affiche le code source colorisé.
',
	'preview_background' => 'Fond de prévisualisation',
	'preview_background_help' => 'Permet de choisir une couleur de fond différente de la couleur du fond de page pour la prévisualisation des composants.',
	'public' => 'Public',

	// R
	'references_autres_composants' => 'Valeurs par défaut',
	'require' => '@class@ <b>@version@</b> nécessite',
	'restore' => 'Restaurer la configuration ACS',
	'restored' => 'L’archive @file@ a été restaurée.',

	// S
	'sauvegarde_trop_ancienne' => 'La dernière sauvegarde a plus de 24 heures.',
	'save' => 'Sauvegarder la configuration ACS',
	'schema' => 'Schéma',
	'set' => 'Set',
	'set_actif' => 'Jeu de composants ACS actif : <b>@set@</b>',
	'set_help' => 'Un set est un jeu de squelettes Spip basés sur des composants ACS.',
	'source' => 'Source',
	'source_page' => 'Code source',
	'spip_admin_form_style' => 'Style du formulaire admin de SPIP',
	'spip_admin_form_style_help' => 'Le formulaire admin de SPIP, visible quand le cookie de correspondance est activé, peut parfois gêner. On peut définir ici des propriétés de style css pour le positionner au mieux. Exemple : right : 100px',
	'squelettes_over_help' => 'Squelette(s) est optionnel, et sert à surcharger le modèle et/ou ses composants. Pour avoir plusieurs niveaux d’override, on sépare les chemins par deux points (<b> :</b>). Les composants ACS des squelettes d’override surchargent ceux du set actif.',
	'structure_page' => 'Structure de la page',

	// T
	'theme' => 'Thème',
	'theme_custom' => 'Personnalisé',
	'themes' => 'Thèmes',
	'themes_change_alert' => 'EFFACE vos personnalisations.',
	'themes_help' => 'Thèmes prédéfinis des jeux de composants.',

	// U
	'undefined' => 'Non défini',
	'used_in' => 'Utilisé dans',

	// V
	'variable' => 'Variable',
	'variables' => 'Variables',
	'variables_all' => 'Toutes les variables',
	'variables_defined' => 'Définies',
	'variables_fantomes' => 'Variables fantômes',
	'variables_fantomes_help' => 'Les variables fantômes ont été définies par des composants supprimés ou modifiés.',
	'variables_undefined' => 'Non définies',
	'voir_pages_composants' => 'Pages des composants',
	'voir_pages_composants_help' => 'Affiche les pages des composants dans l’onglet &quot;Pages&quot; (squelette <i>un_composant</i>.html).',
	'voir_pages_preview_composants' => 'Pages de prévisualisation',
	'voir_pages_preview_composants_help' => 'Affiche les pages de prévisualisation des compsants dans l’onglet &quot;Pages&quot; (squelette <i>un_composant</i>_preview.html).'
);
