<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/acs-paquet-xml-acs?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'acs_description' => 'ACS enable site design by configurable components with "pencils" to change page composition, styles, and colors as easily than SPIP’s "crayons" change a text.',
	'acs_nom' => 'ACS',
	'acs_slogan' => 'Site design wizard'
);
