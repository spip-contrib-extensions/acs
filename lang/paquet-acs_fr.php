<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/acs.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'acs_description' => 'ACS sert à dessiner le site avec des composants configurables par le webmestre à l’aide de "pinceaux" qui permettent de changer mises en page, styles et couleurs aussi facilement que les "crayons" SPIP permettent de changer un texte.',
	'acs_nom' => 'ACS',
	'acs_slogan' => 'Assistant de Conception du Site'
);
