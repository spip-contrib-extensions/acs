<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/adminacs-acs?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'acs' => 'ACS',
	'acs_description' => 'Site design wizard',
	'acs_info' => 'Any used component can be embedded in customized override skeletons which can add their own components.',
	'acs_info_title' => 'Customization',
	'acsdernieremodif' => 'Updated',
	'adm' => 'Administration',
	'admins' => 'Administrators',
	'afterUpdate_not_callable' => 'afterUpdate method not found',
	'aucune_sauvegarde' => 'No backups',

	// B
	'boucle' => 'Loop',

	// C
	'choix_couleur' => 'Color choice',
	'choix_image' => 'Choose an image',
	'composant' => 'Component',
	'composant_non_utilise' => 'Unused component.',
	'composants' => 'Components',
	'creer_composant' => 'Create a new instance of these component',

	// D
	'del_composant' => 'Delete these instance',
	'del_composant_confirm' => 'Would you really delete PERMANENTLY the instance @nic@ of component @c@ ?',
	'dev_infos' => 'Developper infos',
	'documentation' => 'Documentation',

	// E
	'echec_afterUpdate' => 'afterUpdate failure',
	'ecrire' => 'Ecrire',
	'effacer_image' => 'DEFINITIVELY delete image "@image@" from server ???',
	'effacer_variables_fantomes' => 'Delete ghost variables',
	'err_aucun_composant' => 'No active component for ',
	'err_cache' => 'Unable to read or write ACS cache',
	'err_del_file' => 'Unable to delete file',
	'err_fichier_absent' => 'File @file@ not found',
	'err_fichier_ecrire' => 'Unable to write in &quot;@file@&quot;',
	'error_no_set' => 'No set',

	// F
	'formulaire' => 'Form',
	'formulaires' => 'Forms',

	// G
	'groupes' => 'Groups',

	// I
	'impossible_ouvrir_dossier' => 'Unable to open directory',

	// M
	'modele' => 'Model',
	'modeles' => 'Models',

	// N
	'not_found' => 'not found',

	// O
	'onglet_pages_help' => 'ACS add to spip customizables pages made with customizables components.
Click on the little black triangle to display a more detailed schema.

<b>Source</b> display colorized source code of the page.
',
	'onglet_pages_info' => 'In pages list, underlined ones are read in the <span style=&quot;color: darkgreen; font-weight: normal; font-style: normal; text-decoration: underline&quot;>override</span> directory, bolded in current <span style=&quot;color: darkgreen; font-weight: bold; font-style: normal; text-decoration: none&quot;>ACS set</span>, italicized from <span style=&quot;color: darkgreen; font-weight: normal; font-style: underline; text-decoration: none&quot;>plugins</span>, and thoses without any font decoration from <span style=&quot;color: darkgreen; font-weight: normal; font-style: normal; text-decoration: none&quot;>spip distribution</span>.',
	'overriden_by' => ', overriden by skeletons from <u>@over@</u>',

	// P
	'page' => 'Page',
	'page_rien_a_signaler' => 'No variables, no loops, no inclusions.',
	'pages' => 'Pages',
	'preview_background' => 'Preview background',
	'preview_background_help' => 'Choose another background color than the page background for components previews.',
	'public' => 'Public',

	// R
	'references_autres_composants' => 'Default values',
	'require' => 'require',
	'restore' => 'Restore ACS config',
	'restored' => 'Archive @file@ restored.',

	// S
	'sauvegarde_trop_ancienne' => 'Last backup done more than 24 hours ago.',
	'save' => 'Save ACS config',
	'schema' => 'Schema',
	'set' => 'Set',
	'set_actif' => 'ACS active set: <b>@set@</b>',
	'set_help' => 'A set is a Spip skeleton made with ACS components.',
	'source' => 'Source',
	'source_page' => 'Source code',
	'spip_admin_form_style' => 'SPIP admin form style',
	'spip_admin_form_style_help' => 'The "formulaire admin" of SPIP, visible when the "cookie of correspondance" is activated, can be displayed over important elements. You can set here CSS style properties to position it. Sample: right : 100px',
	'squelettes_over_help' => 'Skeleton(s) are optionnal, used to override the set an/or some of its components. To set several override levels, separate paths with "<b>:</b>". ACS components from override paths override components from ACS and plugins sets.',
	'structure_page' => 'page structure',

	// T
	'theme' => 'Skin',
	'theme_custom' => 'Customized',
	'themes' => 'Themes',
	'themes_change_alert' => 'DELETE your customizations.',
	'themes_help' => 'Predefined themes from components sets.',

	// U
	'undefined' => 'Not defined',
	'used_in' => 'Used in',

	// V
	'variable' => 'Variable',
	'variables' => 'Variables',
	'variables_all' => 'All variables',
	'variables_defined' => 'Defined',
	'variables_fantomes' => 'Ghost variables',
	'variables_fantomes_help' => 'Ghost variables was defined by modified or suppressed components.',
	'variables_undefined' => 'Undefined',
	'voir_pages_composants' => 'Components pages',
	'voir_pages_preview_composants' => 'Preview pages'
);
