<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * - Cache le résultat de fonctions qui prennent beaucoup de temps
 * La lecture du cache est parfois des centaines de fois plus rapide.
 * Le recalcul se fera après tout changement valide dans ACS,
 * ou lorsque l'option $force_recalcul vaut true.
 *
 * - Cache result of time-expensive functions. Reading cache is sometimes hundreds
 * time faster. Cache is refreshed after all valid change in ACS configuration,
 * or when $force_recalcul option is true.
 */
function acs_cache($fonction, $file, $args = [], $force_recalcul = false) {
	$cachedir = _ACS_DIR_SITE_ROOT . _NOM_TEMPORAIRES_INACCESSIBLES . 'cache/acs/';
	$cachefile = $cachedir . $file;
	$date = acs_get('ACS_MAJ');
	if (is_readable($cachefile) && !$force_recalcul) {
		$cache = file_get_contents($cachefile);
		$r = unserialize($cache);
		if ($date == $r['date']) {
			return [$r[
				'content'],
				'read',
				$r['date']
			];
		}
	}

	if (!is_callable($fonction)) {
		return [_T('err_not_callable') . ' : ' . $fonction, 'err', $date];
	}
	if (isset($args) && (!is_array($args))) {
		return [_T('err_args_not_in_array') . ' : ' . $fonction, 'err', $date];
	}

	$r = call_user_func_array($fonction, $args);
	acs_log('(' . $fonction . ', ' . $file . ', [' . implode(', ', $args) . '], ' . $force_recalcul . ') in ' . $cachefile . ', time=' . $date, _LOG_DEBUG);
	$cachestring = serialize([
		'date' => $date,
		'content' => $r
	]);
	if (@file_put_contents($cachefile, $cachestring)) {
		return [$r, 'write', $date];
	}
	// Si <site>/tmp/cache/acs n'est pas accessible en écriture, on le crée et on ré-éssaie
	if (is_writable(substr($cachedir, 0, -4))) {
		@mkdir($cachedir);
		if (@file_put_contents($cachefile, $cachestring)) {
			return [$r, 'writedir', $date];
		}
	}
	return [$r, 'err', $date];
}
