<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lit, écrit, ou efface une variable
 */
function acs_get($var) {
	if (isset($GLOBALS['meta'][$var])) {
		return $GLOBALS['meta'][$var];
	}	else {
		return false;
	}
}
function acs_set($var, $value) {
	if ($value && strlen($value) > 0) {
		ecrire_meta($var, $value);
	}
	else {
		effacer_meta($var);
	}
}
function acs_unset($var) {
	acs_set($var, null);
}
/**
 * Force un recalcul des pages, CSS et scripts mis en cache
 */
function acs_recalcul() {
	acs_set('ACS_MAJ', time()); // force un recalcul du cache ACS
	touch_meta(false); // Force la reecriture du cache SPIP des metas
}

	/**
 * Journalise les actions du plugin ACS si _ACS_LOG est supérieur à 0 dans acs_options.php
 * @param txt texte à journaliser
 * @param gravite cf. acs_options.php
 */
function acs_log($txt, $gravite = _LOG_HS) {
	if (_ACS_LOG > _LOG_HS) {
		spip_log(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'] . (substr($txt, 0, 1) === '(' ? '' : ' ') . $txt, 'acs', _LOG_FILTRE_GRAVITE);
	}
}
/**
 * Lit une variable dans un tableau clés-valeurs à la façon de l'API cfg, suivant un chemin,
 * avec en plus la récursivité ACS (une variable ACS peut référencer une autre variable ACS)
 *
 * @param src tableau source
 * @param meta nom de la variable meta à lire
 * @return valeur de la variable (récursive)
 */
function var_recursive($src, $meta, $iterations = 0) {
	static $rec = 0;
	if ($iterations > 20) {
		acs_log('> 20 iterations: ' . $meta, _LOG_AVERTISSEMENT);
		return null;
	}
	$iterations++;
	// Aucun système de cache ou de variable statique ici, ça ralentit
	$chemin = strtok($meta, '/');
	if (!isset($src[$chemin])) {
		return null;
	}
	$val = $src[$chemin];
	// Si la valeur commence par "=", c'est une référence récursive à une autre variable ACS
	if (substr($val, 0, 1) == '=') {
		$val = var_recursive($src, substr($val, 1) . substr($meta, strlen($chemin)), $iterations);
	}
	// On appelle récursivement la fonction tant que $val est un array ou un array serialisé
	if (is_array($val)) {
		$val = var_recursive($val, substr($meta, strlen($chemin) + 1), $iterations);
	}
	elseif (is_array(unserialize($val))) {
		$val = var_recursive(unserialize($val), substr($meta, strlen($chemin) + 1), $iterations);
	}
	return $val;
}
/**
 * Retourne un objet ou un tableau sous forme de tableau affichable en html
 * @param r : objet ou tableau
 * @param html retourne du html si vrai
 * @return objet ou tableau en mode texte ou html lisible
 */
function dbg($r, $html = false) {
	if (is_object($r) or is_array($r)) {
			ob_start();
			print_r($r);
			$r = ob_get_contents();
			ob_end_clean();
			if ($html) {
				$r = htmlentities($r);
			}
			$srch = ['/Array[\n\r]/', '/\s*[\(\)]+/', '/[\n\r]+/', '/ (?= )/s'];
			$repl = ['', '', "\n" , ($html ? '&nbsp;' : ' ')];
			$r = preg_replace($srch, $repl, $r);
			if ($html) {
				$r = nl2br($r);
			}
	}
	return $r;
}
