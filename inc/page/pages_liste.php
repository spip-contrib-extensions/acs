<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne un tableau des pages squelettes du site (Utilise le cache ACS)
 *
 * Spip va chercher ses squelettes dans l'ordre suivant :
 * over > modèle ACS actif > plugins actifs > spip (dist)
 */
function pages_liste() {
	static $liste = [];

	if (!count($liste)) {
		include_spip('inc/acs_cache');
		$set = (acs_get('ACS_SET') ? acs_get('ACS_SET') : 'cat');
		$liste =	acs_cache(
			'pages_du_site',
			'a_' . $set . '_pages_liste'
		);
	}
	return $liste[0];
}

function pages_du_site() {
	$pages = [];

	acs_log('pages_liste.php pages_du_site() $GLOBALS[\'dossier_squelettes\']=' . $GLOBALS['dossier_squelettes'], _LOG_DEBUG);

	// Ordre d'override : over 1 > ...> over n > sets ACS > plugins actifs > spip (squelettes-dist)
	$over = explode(':', acs_get('ACS_OVER'));
	$tas = explode(':', $GLOBALS['dossier_squelettes']);
	$numset = 0;
	foreach ($tas as $dir) {
		if (in_array($dir, $over)) {
			$squelettes['over' . $numover] = _ACS_PLUGIN_SITE_ROOT . $dir;
			$numover++;
		} else {
			$squelettes['acs' . $numset] = _ACS_PLUGIN_SITE_ROOT . $dir;
			$numset++;
		}
	}

	// On ajoute les squelettes de plugins actifs - Add skeletons from active plugins
	$plugins = unserialize($GLOBALS['meta']['plugin']);
	foreach ($plugins as $name => $plugin) {
		if ($name != 'ACS') {
			$squelettes['plugin_' . $name] = _DIR_PLUGINS . $plugin['dir'];
		}
	}

	// On ajoute les squelettes non surchargés de la distribution puisqu'ils sont dans le CHEMIN SPIP.
	$squelettes['spip'] = _DIR_RACINE . 'squelettes-dist';

	foreach ($squelettes as $source => $dir) {
		foreach (pages_du_squelette($dir) as $dossier => $pdd) {
			if (!isset($pages[$dossier])) {
				$pages[$dossier] = [];
			}
			foreach ($pdd as $page => $param) {
				if (!isset($pages[$dossier][$page])) {
					$pages[$dossier][$page] = ['source' => $source];
				}
			}
		}
	}
	//acs_log('pages_liste.php pages_du_site() $pages='.dbg($pages), _LOG_DEBUG);
	return $pages;
}

function pages_du_squelette($dir) {
	$dossiers = ['', 'modeles', 'formulaires'];
	$pages = [];

	if (acs_get('ACS_VOIR_PAGES_COMPOSANTS')) {
		$dossiers_composants = array_keys(composants_liste());
		foreach ($dossiers_composants as $k => $v) {
			$dossiers_composants[$k] = 'composants/' . $v;
		}
		sort($dossiers_composants);
		$dossiers = array_merge($dossiers, $dossiers_composants);
	}
	foreach ($dossiers as $dossier) {
		$pages[$dossier] = [];
		$pdd = pages_du_dossier($dir, $dossier);
		foreach ($pdd as $page => $param) {
			$pages[$dossier][$page] = true;
		}
	}
	//acs_log('pages_liste.php pages_du_squelette() $pages='.dbg($pages), _LOG_DEBUG);
	return $pages;
}

function pages_du_dossier($dir, $dossier) {
	$pages = [];
	$dir .= (($dossier != '') ? '/' . $dossier : '');
	if (@is_dir($dir) and @is_readable($dir) and $d = @opendir($dir)) {
		$nbfiles = 0;
		while (($f = readdir($d)) !== false && ($nbfiles < 1000)) {
			if ($f[0] != '.' and $f != 'remove.txt' and @is_readable($p = "$dir/$f")) {
				if (is_file($p)) {
					if (preg_match(';.*[.]html$;iS', $f)) {
						$pagename = substr($f, 0, -5);
						if (
							($pagename == 'wrap') ||
							($pagename == 'acs.js') ||
							((substr($pagename, -8) == '_preview') && (!acs_get('ACS_VOIR_PAGES_PREVIEW')))
						) {
							continue;
						}
						$pages[$pagename] = true;
					}
				}
			}
			$nbfiles++;
		}
	}
	ksort($pages);
	acs_log('pages_liste.php pages_du_dossier(' . $dir . ', ' . $dossier . ') $pages=' . dbg($pages), _LOG_DEBUG);
	return $pages;
}
