<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/acs_page_get_infos');
include_spip('inc/acs_widgets');

function acs_pages($page) {
	return '<div id="page_infos"><a name="page_infos"></a>' .
		acs_page_get_infos($page, _request('mode'), _request('detail')) .
		'</div>';
}

function acs_pages_droite($page) {
	return acs_vue('acsBox', [
		'titre' => _T('adminacs:acs'),
		'titre2' => '<img src="' . _DIR_ACS . 'images/info.png" alt="&#9432;" />',
		'contenu' => '<div>' . _T('adminacs:set_actif', ['set' => acs_get('ACS_SET')]) .
		((acs_get('ACS_OVER') ? _T('adminacs:overriden_by', ['over' => str_replace(':', ' ', acs_get('ACS_OVER'))]) : '')) . '</div><br><div class="onlinehelp">' . _T('adminacs:onglet_pages_info') . '</div>',
		'class' => '',
		'style' => '',
		'icon' => _DIR_ACS . 'images/acs_32x32.gif'
	]);
}

/**
 * Lit la liste des pages, modèles, et formulaires
 */
function liste_pages_du_site($onglet, $large = false) {
	include_spip('inc/page/pages_liste');

	$r = '';
	if ($large) {
		$r = '<table width="100%" class="liste_pages">';
	}
	foreach (pages_liste() as $dir => $pages) {
		$misenpage = [];
		$misenpage['pg'] = [];
		$misenpage['inc'] = [];
		foreach ($pages as $pagename => $pageparam) {
			$link = (($dir != '') ? $dir . '/' : '') . $pagename;

			$link = '<a class="page_lien nompage" href="?exec=acs&onglet=' . $onglet . '&pg=' . $link . '" title="' . $link . '">';
			if (substr($pageparam['source'], 0, 4) == 'over') {
				$page = $link . '<u>' . $pagename . '</u></a>'; // Highlight override
			}
			elseif (substr($pageparam['source'], 0, 3) == 'acs') {
				$page = $link . '<b>' . $pagename . '</b></a>';
			}
			elseif (substr($pageparam['source'], 0, 7) == 'plugin_') {
				$page = $link . '<i>' . $pagename . '</i></a>';
			} else {
				$page = $link . $pagename . '</a>';
			}
			if (substr($pagename, 0, 4) == 'inc-') {
				$misenpage['inc'][] = $page;
			} else {
				$misenpage['pg'][] = $page;
			}
		}
		if (count($misenpage['pg']) > 0) {
			if ($large) {
				$r .= '<tr><td>';
			}
			$r .= '<span class="onlinehelp">' . _T('adminacs:' . ($dir ? $dir : 'pages')) . '</span>';
			if ($large) {
				$r .= '</td><td style="padding-left: 5px;"> ';
			} else {
				$r .= '<br>';
			}
			$r .= implode(' ', $misenpage['pg']);
			if ($large) {
				$r .= '</td></tr>';
			}	else {
				$r .= '<br>';
			}
		}
		if (count($misenpage['inc']) > 0) {
			if ($large) {
				$r .= '<tr><td>';
			}
			$r .= '<span class="onlinehelp">' . _T('adminacs:includes') . '</span>';
			if ($large) {
				$r .= '</td><td style="padding-left: 5px;"> ';
			} else {
				$r .= '<br>';
			}
			$r .= implode(' ', $misenpage['inc']);
			if ($large) {
				$r .= '</td></tr>';
			} else {
				$r .= '<br>';
			}
		}
		if ($large) {
			$r .= '<tr class="liste_pages_sep"><td colspan="2"></td></tr>';
		} else {
			$r .= '<br>';
		}
	}
	if ($large) {
		$r .= '</table>';
	}
	return $r;
}
