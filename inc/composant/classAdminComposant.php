<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Classe CEdit
 *
 * Chaque composant ACS peut définir une classe MonComposant
 * qui étend la classe CEdit en définissant des méthodes de l'interface ICEdit
 * Ce sont des points d'entrée logiciels pour les objets de classe CEdit,
 * un peu comparables aux pipelines spip, mais en technologie objet explicite
 */
abstract class CEdit implements ICEdit {
}

interface ICEdit {
	public function update();
}

/**
 * Interface d'admin de composants
 *
 * Instancie (au besoin) un objet CEdit à l'exécution
 * pour en adopter les méthodes implémentées.
 * @param string $class classe du composant
 * @param integer $nic numero d'instance du composant
 */
class AdminComposant {
	public function __construct($class, $nic = 0) {
		global $_POST;

		require_once(_DIR_ACS . 'inc/acs_version.php');

		$this->class = $class; // Classe de composant
		$this->nic = $nic != 0 ? $nic : ''; // Numéro d'instance du composant
		$this->fullname = 'acs' . ucfirst($class) . $this->nic;
		$this->urledit = generer_url_ecrire('acs', 'onglet=composants&composant=' . $this->class . ($this->nic ? '&nic=' . $this->nic : ''));
		$this->errors = [];
		$this->instances = [];
		$this->vars = [];
		$this->cvars = []; // Variables issues d'un autre composant
		$this->necessite = []; // Dependances du composant (version spip, plugins et composants)
		$this->necessite['plugin'] = []; // Dependances du composant (plugins)
		$this->necessite['composant'] = []; // Dependances du composant (composants)
		$this->nb_widgets = 0; // Nb de variables de type widget
		$this->optionnel = 'oui';
		$this->instanciable = false;
		$this->enable = true;
		$this->preview_type = 'iframe';

		$cc = composants_lire()[$this->class];

		$this->root = _ACS_PLUGIN_SITE_ROOT . $cc['path']; // Dossier racine du composant
		$this->icon = $this->root . '/images/' . $class . '_icon.gif';
		if (!is_readable($this->icon)) {
			$this->icon = _DIR_ACS . '/images/composant-24.gif';
		}
		// Affecte sa configuration à l'objet Composant
		foreach (['nom', 'group', 'version', 'vars', 'nb_widgets', 'cvars', 'instances'] as $cp) {
			if (isset($cc[$cp])) {
				$this->$cp = $cc[$cp];
			}
		}
		// Lit les dépendances (plugins: necessite, composants: necessite_composant)
		if (isset($cc['necessite'])) {
			$this->necessite = $cc['necessite'];
		}
		// Lit les paramètres
		if (isset($cc['param'])) {
			foreach ($cc['param'] as $param => $pval) {
				$this->$param = $pval;
			}
		}

		// Désactive le composant s'il dépend de plugins non activés (ou obsolètes TODO)
		foreach ($this->necessite['plugin'] as $plug) {
			if (isset($plug['nom'])) {
				$plugin = $plug['nom'];
			} elseif (isset($plug['id'])) {// Compat anciennes versions
				$plugin = $plug['id'];
			}
			//$version = $plug['version'];
			if (!acs_get_from_active_plugin(strtoupper(trim($plugin)))) {
				$this->enable = false;
			}
		}
		// Active le composant non optionnel ou nouvellement crée, si nécessaire
		if (in_array($this->optionnel, ['non', 'no', 'false']) || (_request('create_instance_' . md5($this->fullname)) === 'create')) {
			if (acs_get($this->fullname . 'Use') != 'oui') {
				acs_set($this->fullname . 'Use', 'oui');
				$this->enable = true;
				$updated = true;
			}
		}
		elseif (
			isset($this->optionnel) &&
			!in_array($this->optionnel, ['oui', 'yes', 'true']) &&
			(acs_get($this->optionnel) != 'oui')
		) {
			// Regarde si le composant optionnel doit être activé
			// Désactive le composant si "optionnel" est égal à une variable de configuration non égale à "oui"
			// (si optionnel ne vaut pas oui, yes, ou true, il s'agit d'un nom de variable meta. Exemple: activer_breves)
			acs_set($this->fullname . 'Use', 'non');
			$this->enable = false;
			$updated = true;
		}

		$this->vars['Use'] = [
			'type' => 'use',
			'valeur' => acs_get($this->fullname . 'Use')
		];

		if (_ACS_LOG >= _LOG_DEBUG) {
			$this->errors[] = 'Debug mode _ACS_LOG = ' . _ACS_LOG . ' in acs_options.php<br>';
		// Mise à jour
			if ($_POST) {
				$this->errors[] = "\n<br>\$_POST=\n";
				$this->errors[] = dbg($_POST, 'html');
				$this->errors[] = "\n<br>\n";
			}
		}
		// Création d'une instance
		if (_request('create_instance_' . md5($this->fullname)) === 'create') {
			$this->instances[$this->nic] = ['on' => 'oui'];
		}
		// Suppression de l'instance
		if (_request('del_composant_' . md5($this->fullname)) === 'delete') {
			foreach ($this->vars as $varnom => $var) {
				$v = $this->fullname . $varnom;
				effacer_meta($v);
			}
			unset($this->instances[$this->nic]);
			$this->nic = '';
			$this->fullname = 'acs' . ucfirst($this->class);
			$this->urledit = generer_url_ecrire('acs', 'onglet=composants&composant=' . $this->class);
			$updated = true;
		}
		// Mise à jour
		if (_request('maj_composant_' . md5($this->fullname)) == 'oui') {
			foreach ($this->vars as $varnom => $var) {
				unset($nv);
				$v = $this->fullname . $varnom;
				switch ($var['type']) {
					case 'bord':
						// on ne traite surtout pas les variables non postées
						if (!isset($_POST[$v . 'Color'])) {
							continue 2;
						}
						if ($_POST[$v . 'Color'] === '') {
							$nv = '';
						}
						else {
							$posted_color = $_POST[$v . 'Color'];
							// si la valeur postee commence par "=", c'est une référence à la valeur d'un autre composant
							if (substr($posted_color, 0, 1) == '=') {
								$nv = $posted_color;
							}
							else {
								$nv = [
									'Width' => $_POST[$v . 'Width'],
									'Style' => $_POST[$v . 'Style'],
									'Color' => $_POST[$v . 'Color']
								];
							}
						}
						if (is_array($nv)) {
							$nv = serialize($nv);
						}
						break;

					case 'key':
						$group = $_POST[$v . 'Group'];
						$key = $_POST[$v . 'Key'];
						$nv = serialize(['Group' => $group, 'Key' => $key]);
						break;

					default:
						// on ne traite surtout pas les variables non postées
						if (!isset($_POST[$v])) {
							continue 2;
						}
						$nv = $_POST[$v];
				}

				// On continue si rien à faire
				if ($nv == acs_get($v)) {
					continue;
				}

				// Si la nouvelle valeur est vide
				if ($nv === '') {
					// et que la variable n'a jamais été initialisée,
					if ((null !== $var['valeur']) && (null !== acs_get($v))) {
						// on lui affecte la valeur par défaut
						if (substr($var['valeur'], 0, 4) == '=acs') {
							$nv = acs_get($var['valeur']);
						} else {
							$nv = $var['valeur'];
						}
					}
				}

				// Mise à jour
				acs_set($v, $nv);
				$updated = true;
				acs_log('AdminComposant updated: $v = ' . $v . ', $nv = ' . $nv, _LOG_DEBUG);
			}
		}
		if (isset($updated)) {
			if (isset($this->update)) {
				include_spip('composants/' . $class . '/ecrire/' . $class);
				$c_obj = 'acs' . ucfirst($class) . 'Edit';
				if (class_exists($c_obj)) {
					$$c_obj = new $c_obj();
					if (($$c_obj instanceof CEdit) && is_callable([$$c_obj, 'update'])) {
						if (!$$c_obj->update()) {
							$this->errors[] = $c_obj . '->update ' . _T('adminacs:failed') . ' ' . implode(' ', $$c_obj->errors);
						}
					} else {
						$this->errors[] = $c_obj . '->update ' . _T('adminacs:not_callable');
					}
				}	else {
					$this->errors[] = $c_obj . '->update ' . _T('adminacs:not_found');
				}
			}
			acs_recalcul();
			unset($updated);
		}
		acs_log('AdminComposant(' . $class . ', ' . $nic . '), _ACS_LOG=' . _ACS_LOG, _LOG_DEBUG);
	}

/**
 * Méthode privée _getCvarsHtml: retourne du code html pour les variables du composant
 * faisant référence à une variable définie par un autre composant
 * pour leurs valeurs par défaut
 */
	private function _getCvarsHtml() {
		foreach ($this->cvars as $k => $var) {
			if (null !== acs_get($var)) {
				$class = ' alert';
			} else {
				$class = '';
			}
			$this->cvars[$k] =
			'<a class="nompage' . $class . ' acsLabel" title="' . $var . '=' . acs_get($var) . '">' .
				substr($this->cvars[$k], 3) . '</a>';
		}
		return implode(', ', $this->cvars);
	}

	public function t($txt) {
		return str_replace('"', '&quot;', _T('acs:' . $this->class . '_' . $txt));
	}

/**
 * Méthode infos: affiche des infos pour developpeur
 * @return string html code
 */
	public function infos() {
		global $spip_version_code;
		$r = '';
		if ($this->t('description') != 'description') {
			$r .= '<div>' . $this->t('description') . '</div><br>';
		}

		if ($this->t('info') != 'info') {
			$r .= '<div class="onlinehelp" style="text-align: justify">' . $this->t('info') . '</div><br>';
		}

		$n = uniqid();
		$r .= '<div class="onlinehelp">' .
		acs_plieur(
			'plieur_pu' . $n,
			'pu' . $n,
			'#',
			false,
			'if (!this.getAttribute(\'data-acs-ajax-loaded\')) {
	let clicked = this;
	aSqueezeNode(\'' . generer_url_ecrire('composant_get_infos', '&c=' . $this->class . ($this->nic ? '&nic=' . $this->nic : '')) . '\',
	jQuery(\'#puAjax' . $n . '\'),
	function() {
		clicked.setAttribute(\'data-acs-ajax-loaded\', true);
		jQuery(\'.\' + clicked.name.substr(7)).slideToggle(\'slow\');
	});
}',
			_T('adminacs:dev_infos')
		) .
		'</div><div class="pu' . $n . '">';

		if (count($this->cvars)) {
			$r .= '<br><div class="onlinehelp">' . _T('adminacs:references_autres_composants') .
				'</div><div class="onlinehelplayer">' . $this->_getCvarsHtml() . '</div>';
		}
		$r .= '<div id="puAjax' . $n . '" class="puAjax' . $n . '"></div>';
		$r .= '</div>';
		return $r;
	}

/**
 * Méthode edit: affiche un editeur pour les variables du composant
 * @param form : paramètres de formulaire
 * @param mode : mode d'affichage (espace prive ou controleur)
 * @return string html code
 */
	public function edit($form = false, $mode = false) {
		$r = '';
		if (!is_array($form)) {
			$form = [
				'action' => $this->urledit,
				'inputs' =>
					'<input type="hidden" name="maj_composant_' . md5($this->fullname) . '" value="oui" />',
				'buttons' => '<hr /><div style="text-align:' . $GLOBALS['spip_lang_right'] . ';">' .
					'<input type="submit" class="fondo" name="' . _T('bouton_valider') . '" value="' . _T('bouton_valider') . '"></div>'
			];
		}
		include_spip('public/assembler');
		include_spip('inc/composant/classControles');

		if (($mode != 'controleur') && !in_array($this->optionnel, ['non', 'no', 'false']) && (count($this->instances) < 2)) {
			$varname = $this->fullname . 'Use';
			if ((null !== acs_get($varname)) && acs_get($varname)) {
				$var = acs_get($varname);
			} else {
				$var = 'non';
			}
			$this->display = 'display: ' . ($var == 'oui' ? 'block' : 'none');
			$nc = $this->t('nom');
			if ($nc == str_replace('_', ' ', $this->t('nom'))) {
				$nc = ucfirst($this->nom);
			}
			if (!$nc) {
				$nc = ucfirst($this->class);
			}
			$ctl_use = '<div align="' . $GLOBALS['spip_lang_right'] . '"><label>' . _T('varacs:use') . ' ' . $nc . ' ' . $this->nic . ' : </label>';
			// NB: acs_bouton_radio($nom, $valeur, $titre, $actif=false, $onClick="", $enable=true)
			$ctl_use .= acs_bouton_radio($varname, 'oui', _T('varacs:oui'), $var == 'oui');
			$ctl_use .= acs_bouton_radio($varname, 'non', _T('varacs:non'), $var == 'non');
			$ctl_use .= '</div>';
		}
		if (($mode != 'controleur') && isset($this->preview) && ($this->preview != 'non')	&& ($this->preview != 'no') && ($this->preview != 'false')) {
			$url = '../?page=wrap&c=composants/' . $this->class . '/' . $this->class . ($this->nic ? '&amp;nic=' . $this->nic : '') . '&v=' . acs_get('ACS_MAJ') . '&var_mode=recalcul';
			switch ($this->preview_type) {
				case 'inline':
					 $preview = '<script src="../spip.php?page=acs.js"></script><link rel="stylesheet" href="../spip.php?page=' . acs_get('ACS_SET') . '.css" type="text/css" media="screen" /><div id="' . $this->fullname . '" style="border:0;overflow: auto; width: 100%; height: ' . (is_numeric($this->preview) ? $this->preview : 80) . 'px">' . recuperer_fond('vues/composant', [
					 'c' => 'composants/' . $this->class . '/' . $this->class,
					 'nic' => $this->nic,
					 'lang' => $GLOBALS['spip_lang']
					 ]) . '</div>';
					break;
				default:
					$preview = '<iframe id="' . $this->fullname . '" width="100%" height="' . (is_numeric($this->preview) ? $this->preview : 80) . 	'px" frameborder="0" style="border:0; background:' . acs_get('acsFondColor') . '" src="' . $url . '"></iframe>';
			}
			$r .= '<fieldset class="apercu"><legend><a href="javascript:void(0)" onclick="document.getElementById(\'' . $this->fullname . '\').src=\'' . $url . '\';return false;" title="' . _T('admin_recalculer') . '">' . $this->t('previsualisation') . '</a></legend>' . $preview . '</fieldset>';
		}
		// fabrique un tableau de contrôles pour les variables paramétrables du composant:
		$controls = [];
		foreach ($this->vars as $varnom => $var) {
			if ($varnom === 'Use') {
				continue;
			}
			$v = $varnom;
			if (null != acs_get($this->fullname . $v)) {
				$$v = acs_get($this->fullname . $v);
			}
			elseif (isset($var['valeur'])) {
				$default = $var['valeur'];
				if ((substr($default, 0, 3) == 'acs') && (null !== acs_get($default))) {
					$$v = acs_get($default);
				}
				elseif (substr($default, 0, 3) != 'acs') {
					$$v = $default;
				}
			}
			$ctl = 'ctl' . ucfirst($var['type']);
			if (class_exists($ctl)) {
				$value = isset($$v) ? $$v : null;
				$ctl = new $ctl($this->class, $this->nic, $v, $value, $var);
				if (method_exists($ctl, 'draw')) {
					$controls[$varnom] = $ctl->draw();
				}
			} else {
				$controls[$varnom] = $ctl . '() undefined.<br>';
			}
		}

		// Recherche une mise en page et y remplace les variables par des contrôles, sinon ajoute les contrôles non mis en page
		if (find_in_path('composants/' . $this->class . '/ecrire/' . $this->class . '_mep.html')) {
			$mep = recuperer_fond('composants/' . $this->class . '/ecrire/' . $this->class . '_mep', ['lang' => $GLOBALS['spip_lang'], 'nic' => $this->nic]);
			foreach ($controls as $nom => $html) {
				$tag = '&' . $nom . '&';
				$mep = str_replace($tag, $html, $mep);
			}
			// en mode controleur
			if ($mode == 'controleur') {
				$mep = preg_replace('%<admin>(.*?)</admin>%s', '', $mep);
				// on ajoute si besoin est la liste des widgets, invisible
				if ($this->nb_widgets > 0) {
					$mep .= liste_widgets(false);
				}
			}
		} else {
			$mep = implode('<br>', $controls);
		}
		if (_request('panel') == 't') {
			require_once(_DIR_ACS . 'inc/composant/composant_traductions.php');
		}
		return acs_vue('acsEdit', [
			'fullname' => $this->fullname,
			'form' => $form,
			'ctlUse' => isset($ctl_use) ? $ctl_use : '',
			'showEditor' => isset($this->display) ? ' style="' . $this->display . '"' : '',
			'editor' => $r,
			'panel' => _request('panel') ? _request('panel') : 'c',
			'mep' => isset($mep) ? $mep : false,
			'traductions' => _request('panel') == 't' ? composant_traductions($this->class) : '',
			'align' => $GLOBALS['spip_lang_right'],
			'mode_controleur' => $mode == 'controleur',
			'infos' => $this->infos()
		]);
	}
/**
 * Méthode tabs: retourne les liens vers les panneaux du composant
 */
	public function tabs($mode = '') {
		$r = '<a href="' . $this->urledit . '&panel=c" class="acsTab' . (_request('panel') !== 'i' && _request('panel') !== 't' ? ' acsTabOn' : '') . '" data-acs-panel="pc_' . $this->fullname . '">' .
			_T('ecrire:icone_edition') . '</a>&nbsp;';
			if ($mode === 'controleur') {
				$r .= '<a href="' . $this->urledit . '&panel=i" class="acsTab' . (_request('panel') == 'i' ? ' acsTabOn' : '') . '" data-acs-panel="pi_' . $this->fullname . '">' .
				_T('ecrire:onglet_proprietes') . '</a>&nbsp;';
			}
			else {
				$r .= '<a href="' . $this->urledit . '&panel=t" class="acsTab' . (_request('panel') == 't' ? ' acsTabOn' : '') .
					'" data-acs-panel="pt_' . $this->fullname .
					'" data-acs-ajax="' . generer_url_ecrire('composant_get_traductions', '&c=' . $this->class) .
					'">' . _T('ecrire:info_traductions') . '</a>&nbsp;';
			}
		return $r;
	}

/**
 * Méthode nextInstance: retourne un numéro d'instance de composant inutilisé
 */
	public function nextInstance() {
		$i = composant_instances($this->class);
		return count($i) + 1;
	}

	/**
	* Méthode _getPages: retourne un tableau des squelettes qui utilisent le composant $nic
	* @param string $chemin chemin relatif d'un set de composants
	* @return array
	*/
	private function _getPages($chemin = '') {
		$c = $this->class;
		$nic = $this->nic;
		$pages = [];
		$pages['composant'] = [];
		$pages['variables'] = [];

		if (!$c) {
			return $pages;
		}
		if ($chemin == '') {
			$dir = _ACS_PLUGIN_SITE_ROOT . acs_get('ACS_SET_DIR');
		} else {
			$dir = find_in_path($chemin);
		}
		if (@is_dir($dir) and @is_readable($dir) and $d = @opendir($dir)) {
			$vars = [];
			foreach (array_keys($this->vars) as $ivar) {
				$vars[] = 'acs' . $this->fullname . $ivar;
			}
			if ($nic) {
				$cpreg = '/\{fond=composants\/' . $c . '\/[^\}]*\}.*\{nic=' . $nic . '\}/';
			} else {
				$cpreg = '/\{fond=composants\/' . $c . '\/[^\}]*\}.^[\{nic=' . $nic . '\}]*/';
			}
			$nbfiles = 0;
			while (($f = readdir($d)) !== false && ($nbfiles < 1000)) {
				if ($f[0] != '.' and $f != 'remove.txt' and @is_readable($p = "$dir/$f")) {
					if (is_file($p)) {
						if (preg_match(';.*[.]html$;iS', $f)) {
							$fic = @file_get_contents($p);
							if (preg_match($cpreg, $fic, $matches)) {
								$pages['composant'][] = substr($f, 0, -5);
							}
							foreach ($vars as $var) {
								if (strpos($fic, $var)) {
									$pages['variables'][substr($f, 0, -5)][] = $var;
								}
							}
						}
					}
				}
				$nbfiles++;
			}
			$pages['chemin'] = $chemin;
		}
		return $pages;
	}

	/**
	 * Retourne toutes les pages des squelettes, composants, et noisettes qui
	 * contiennent l'instance $nic du composant $c
	 */
	public function listePages() {
		$nic = $this->nic;
		$c = $this->class;
		$r = '';
		$dirs = [
			['', _T('adminacs:page'), _T('adminacs:pages')],
			['modeles', _T('adminacs:modele'), _T('adminacs:modeles')],
			['formulaires', _T('adminacs:formulaire'), _T('adminacs:formulaires')]
		];
		foreach (composants_liste() as $class => $composant) {
			$dirs[] = ['composants/' . $class, _T('adminacs:composant'), _T('adminacs:composants')];
		}
		foreach ($dirs as $dir) {
			$p = $this->_getPages($dir[0]);
			if (count($p['composant']) > 0) {
				$r = '<span class="onlinehelp">' . (count($p['composant']) > 1 ? $dir[2] : $dir[1]) . '</span> ';
				foreach ($p['composant'] as $page) {
					$r .= $this->_showOverride($p['chemin'], $page) . ' ';
				}
				$r .= '<br>';
			}
			if (count($p['variables']) > 0) {
				$len = strlen('acs' . $c . $nic);
				$r .= '<i>';
				foreach ($p['variables'] as $page => $var) {
					$r .= $this->_showOverride($p['chemin'], $page) . ' (';
					foreach ($var as $v) {
						$r .= '<a class="acsLabel" title="' . $v . (acs_get($v) ? ' = ' . htmlentities(acs_get($v)) : '') . '">' . substr($v, $len) . '</a> ';
					}
					$r = rtrim($r);
					$r .= ')<br>';
				}
				$r .= '</i>';
			}
		}
		return $r;
	}

	private function _showOverride($chemin, $page) {
		if ($chemin) {
			$chemin .= '/';
		}
		if ((null !== acs_get('ACS_OVER')) && is_readable('../' . acs_get('ACS_OVER') . '/' . $page . '.html')) {
			$r = '<u>' . $page . '</u>';
		}	else {
			$r = $page;
		}
		$r = '&nbsp;&nbsp;&nbsp;<a class="nompage acsLabel" href="' . generer_url_ecrire('acs', '&onglet=pages&pg=' . $chemin . $page . '" title="' . $chemin . $page) . '">' . $r . '</a>';
		return $r;
	}
}
