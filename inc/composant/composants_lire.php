<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/xml');

/**
 * Charge en une seule fois toutes les variables de configuration des composants dans un tableau mis en cache
 * On cherche les composants dans les dossiers composants et sets/nom_du_set/composants accessibles depuis les chemins SPIP
 *
 * @return array
 */
function composants_lire() {
	static $cl = [];

	if (count($cl) > 0) {
		return $cl; // Return result if done once
	}

	require_once _DIR_ACS . 'inc/acs_cache.php';
	$cl = acs_cache(
		'cherche_composants',
		'a_c',
	);
	$cl = $cl[0];
	return $cl;
}
function cherche_composants() {
	$lc = [];
	$nb = 0;
	$dirs = creer_chemin(); // Chemins SPIP, dans l'ordre
	if (acs_get('ACS_OVER')) {
		foreach (explode(':', acs_get('ACS_OVER')) as $path) {
			array_unshift($dirs, $path);
		}
	}
	foreach ($dirs as $dir) {
		$lc = array_merge($lc, lit_composants(_ACS_PLUGIN_SITE_ROOT . $dir . '/composants', $dir));
		$dir_sets = $dir . 'sets';
		if (is_dir($dir_sets) && @is_readable($dir_sets) && ($d = @opendir($dir_sets))) {
			while (($f = readdir($d)) !== false && ($nb < 1000)) {
				if (($f[0] != '.') && is_dir($dir_sets . '/' . $f . '/composants')) {
					$lc = array_merge($lc, lit_composants($dir_sets . '/' . $f . '/composants'));
				}
			}
		}
	}
	return $lc;
}

/*
 * Retourne la liste des composants du dossier $dirc et de leurs instances
 */
function lit_composants($dirc, $overdir = '') {
	$lc = [];
	$nb = 0;
	if (!(@is_dir($dirc) and @is_readable($dirc) and ($d = @opendir($dirc)))) {
		return $lc;
	}
	while (($f = readdir($d)) !== false && ($nb < 1000)) {
		if (($f[0] == '.') || !is_dir($dirc . '/' . $f)) {
			continue;
		}
		$lc[$f]['path'] = (substr($dirc, 0, 3) === '../' ? substr($dirc, 3) : $dirc) . '/' . $f;
		if ($overdir != '') {
			$lc[$f]['over'] = $overdir;
		}
		// On stocke le chemin des balises de composants, en tenant compte d'un possible override
		$balises = $dirc . '/' . $f . '/' . $f . '_balises.php';
		if (is_file($balises)) {
			$lc[$f]['balises'] = str_replace('../', '', $balises);
		}

		$cf = $dirc . "/$f/ecrire/composant.xml";
		if (is_file($cf) && @is_readable($cf)) {
			$config = spip_xml_load($cf); // Lit les paramètres de configuration du composant
			$c = $config['composant'][0];
			foreach (['nom', 'group', 'version'] as $cp) {
				if (isset($c[$cp][0])) {
					$lc[$f][$cp] = $c[$cp][0];
				}
			}
			// Lit les dépendances (plugins: necessite, composants: necessite_composant)
			if (spip_xml_match_nodes(',^necessite,', $config, $needs)) {
				foreach (array_keys($c) as $tag) {
					list($tag, $att) = spip_xml_decompose_tag($tag);
					if ($att) {
						$lc[$f]['necessite'][(isset($att['set']) ? 'composant' : 'plugin')][] = $att;
					}
				}
				krsort($lc[$f]['necessite']); // on met les plugins avant les composants
			}

			// Lecture des paramètres - reading parameters
			if (isset($c['param']) && is_array($c['param'])) {
				foreach ($c['param'] as $param) {
					if (is_array($param) && is_array($param['nom']) && isset($param['valeur']) && is_array($param['valeur'])) {
						$lc[$f]['param'][$param['nom'][0]] = $param['valeur'][0];
					}
					else {
						$lc[$f]['param'][$param['nom'][0]] = true;
					}
				}
			}

			$lc[$f]['vars'] = [];
			$lc[$f]['vars']['Use'] = ['type' => 'use'];
			$lc[$f]['nb_widgets'] = 0;
			$lc[$f]['cvars'] = [];
			// Lecture des variables - reading variables
			if (isset($c['variable']) && is_array($c['variable'])) {
				foreach ($c['variable'] as $k => $var) {
					foreach ($var as $xmltag => $value) {
						if ($xmltag === 'nom') {
							$nom = $value[0];
						}
						elseif (count($value) > 1) {
							$lc[$f]['vars'][$nom][$xmltag] = $value;
						}
						else {
							$lc[$f]['vars'][$nom][$xmltag] = $value[0];
						}
						if (($xmltag === 'type') && ($value[0] === 'widget')) {
							$lc[$f]['nb_widgets']++;
						}
						if ($xmltag == 'valeur') { // Default values
							if (substr($value[0], 0, 4) == '=acs') {
								$cvar = substr($value[0], 1);
								if (!in_array($cvar, $lc[$f]['cvars'])) {
									$lc[$f]['cvars'][] = $cvar;
								}
							}
						}
					}
				}
			}

			$instances = composant_instances($f);
			if (count($instances)) {
				foreach ($instances as $nic) {
					$lc[$f]['instances'][$nic]['on'] = acs_get('acs' . ucfirst($f) . $nic . 'Use');
				}
			}
			else {
				$lc[$f]['instances'][0]['on'] = acs_get('acs' . ucfirst($f) . 'Use');
			}
		} else {
			$lc[$f]['instances'][0]['on'] = acs_get('acs' . ucfirst($f) . 'Use');
		}
		$nb++;
	}
	return $lc;
}

/**
 * Retourne un tableau des sets de composants
 *
 * Utilise le cache ACS
 */

function composants_sets() {
	static $cs = [];
	if (count($cs) > 0) {
		return $cs;
	}
	require_once _DIR_ACS . 'inc/acs_cache.php';
	$cs = acs_cache(
		'lit_composants_sets',
		'a_cs'
	);
	$cs = $cs[0];
	return $cs;
}
function lit_composants_sets() {
	include_spip('inc/traduire');
	include_spip('inc/sp/backport_lire_fichier_langue');
	$sets = [];
	foreach (composants_lire() as $class => $c) {
		$path = substr($c['path'], 0, - strlen($class));
		if (!in_array($path, array_keys($sets))) {
			$path_set = substr($path, 0, -12); // 12 = strlen('/composants/')
			if (strpos($path_set, '/')) {
				$set = substr($path_set, strrpos($path_set, '/') + 1);
			} else {
				$set = $path_set;
			}
			$paquet = _ACS_PLUGIN_SITE_ROOT . $path_set . '/paquet.xml';
			if (is_file($paquet)) {
				if (isset($GLOBALS['_COOKIE']['spip_lang_ecrire'])) {
					$lang = $GLOBALS['_COOKIE']['spip_lang_ecrire'];
				} else {
					$lang = _LANGUE_PAR_DEFAUT;
				}
				$langfile = _ACS_PLUGIN_SITE_ROOT . $path_set . '/lang/paquet-' . $set . '_' . $lang . '.php';
				if (is_file($langfile)) {
					$trads_of_set = lire_fichier_langue($langfile); // Appel fonction SPIP ou backport
				}
			}
			$themes = [];
			$langfile_of_set = false;
			$dir = _ACS_PLUGIN_SITE_ROOT . $path;
			if ($sd = @opendir($dir)) {
				while (false !== ($theme = @readdir($sd))) {
					if ((substr($theme, -4) == '.php') && is_file($dir . $theme)) {
						$nom = substr($theme, 0, -4);
						$themes[$nom] = [
							'nom' => $trads_of_set['theme_' . $nom] ? $trads_of_set['theme_' . $nom] : $nom,
							'tip' => $trads_of_set['theme_' . $nom . '_tip'],
						];
					}
				}
			} else {
				acs_log('inc/composants_lire.php lit_composants_sets() : unable to read ' . $dir, _LOG_ERREUR);
			}
			$sets[$path_set] = [
				'set' => $set,
				'nom' => $trads_of_set[$set . '_nom'] ? $trads_of_set[$set . '_nom'] : $set,
				'slogan' => $trads_of_set[$set . '_slogan'],
				'description' => $trads_of_set[$set . '_description'],
				'themes' => $themes
			];
		}
	}
	return $sets;
}

function composants_balises() {
	static $cb = [];
	if (count($cb) > 0) {
		return $cb;
	}
	$cb = acs_cache(
		'lit_composants_balises',
		'a_cb'
	);
	$cb = $cb[0];
	return $cb;
}
function lit_composants_balises() {
	$cb = [];
	$composants = composants_lire();
	foreach ($composants as $c => $config) {
		if (isset($config['balises'])) {
			$cb[] = $config['balises'];
		}
	}
	return $cb;
}

function composants_variables() {
	static $cv = [];
	if (count($cv) > 0) {
		return $cv;
	}
	$cv = acs_cache(
		'lit_composants_variables',
		'a_cv'
	);
	$cv = $cv[0];
	return $cv;
}
function lit_composants_variables() {
	$cv = [];
	$composants = composants_lire();
	foreach ($composants as $c => $config) {
		if (isset($config['vars'])) {
			$cv[$c]['vars'] = $config['vars'];
		}
		$cv[$c]['vars']['Use'] = ['type' => 'use'];
		if (isset($config['over'])) {
			$cv[$c]['over'] = $config['over'];
		}
	}
	return $cv;
}

function liste_variables() {
	static $lv = [];
	if (count($lv) > 0) {
		return $lv;
	}
	$cv = composants_variables();
	foreach ($cv as $c => $p) {
		foreach ($p['vars'] as $var => $vp) {
			$lv[ucfirst($c) . $var] = ['c' => $c, 'type' => $vp['type']];
		}
		foreach (composant_instances($c) as $nic) {
			foreach ($p['vars'] as $var => $vp) {
				$lv[ucfirst($c) . $nic . $var] = ['c' => $c, 'type' => $vp['type'], 'nic' => $nic];
			}
		}
	}
	return $lv;
}

function composants_liste() {
	static $cl = [];

	if (count($cl) > 0) {
		return $cl; // Return result if done once
	}

	require_once _DIR_ACS . 'inc/acs_cache.php';
	$cl = acs_cache(
		'lit_composants_liste',
		'a_cl',
	);
	$cl = $cl[0];
	return $cl;
}

function lit_composants_liste() {
	$cl = [];
	$composants = composants_lire();
	foreach ($composants as $c => $config) {
		if (isset($config['group'])) {
			$cl[$c]['group'] = $config['group'];
		}
		if (isset($config['over'])) {
			$cl[$c]['over'] = $config['over'];
		}
		$cl[$c]['instances'] = $config['instances'];
	}
	ksort($cl);
	// on met le composant "fond" en premier pour permettre son override par d'autres composants
	if (array_key_exists('fond', $cl)) {
		$fond = $cl['fond'];
		unset($cl['fond']);
		$cl = array_merge(['fond' => $fond], $cl);
	}
	return $cl;
}

/**
 * Retourne les instances d'un composant
 */
function composant_instances($c) {
	static $ci = [];

	if (isset($ci[$c]) && (is_array($ci[$c])) && (count($ci[$c]) > 0)) {
		return $ci[$c];
	}

	$ci[$c] = [];
	$reg = '/acs' . ucfirst($c) . '(\d+)*Use/';
	foreach ($GLOBALS['meta'] as $meta => $val) {
		if (preg_match($reg, $meta, $matches) && isset($matches[1])) {
			$ci[$c][] = $matches[1];
		}
	}
	sort($ci[$c]);
	return $ci[$c];
}

/**
 * Renvoit true si un composant a au moins une instance active
 * Le paramètre $composant est celui d'une boucle foreach (composants_liste() as $class => $composant)
 */
function composant_actif($composant) {
	$actif = false;
	foreach ($composant['instances'] as $c) {
		if ($c['on'] == 'oui') {
			$actif = true;
			break;
		}
	}
	return $actif;
}
