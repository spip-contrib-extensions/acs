<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne un tableau html des traductions disponibles d'un composant
 */

include_spip('inc/composant/classAdminComposant');

function composant_traductions($c) {
	$r['public'] = c_files('composants/' . $c . '/lang', 'php', strlen($c) + 1);
	$r['ecrire'] = c_files('composants/' . $c . '/ecrire/lang', 'php', strlen($c) + strlen('ecrire') + 2);
	if (count($r['public']) || count($r['ecrire'])) {
		$ret .= '<table width="100%">';
		foreach ($r as $pp => $langs) {
			if ($langs && count($langs)) {
				$ret .= '<tr><td style="width:10%; vertical-align: top;" align="' . $GLOBALS['spip_lang_right'] . '"> ' . _T('adminacs:' . $pp) . ' </td><td>&nbsp;</td><td>';
				foreach ($langs as $lang) {
					$context = ($pp == 'public' ? '' : $pp);
					$ret .= ' <a href="' . generer_url_ecrire('acs', 'onglet=composants&composant=' . $c) . '&panel=t&trad=' . $lang . '" title="' . traduire_nom_langue($lang) .
'" onclick="aSqueezeNode(\'' . generer_url_ecrire('composant_get_trad', 'c=' . $c . '&trcmp=' . $lang . '&context=' . $context) . '\', jQuery(\'#pTrad\'), this.setAttribute(\'data-acs-ajax-loaded\', true));
return false;" ><img src="' . _DIR_ACS . 'lang/flags/' . $lang . '.gif" alt="' . $lang . '" /></a> ';
				}
				$ret .= '</td></tr>';
			}
		}
		$ret .= '</table>';
	}
	return $ret;
}

/**
 * Retourne un tableau de fichiers d'un composant
 * dont l'extension vérifie l'expression régulière $ext (par défaut: les fichiers html)
 * @param string $chemin
 * @param string $ext expression régulière
 * @param integer $skip
 */
function c_files($chemin = '', $ext = 'html', $skip = 0) {
	$files = [];
	$dir = find_in_path($chemin);
	if (@is_dir($dir) and @is_readable($dir) and $d = @opendir($dir)) {
		$nbfiles = 0;
		while (($f = readdir($d)) !== false && ($nbfiles < 1000)) {
			// ignorer . ..
			if ($f[0] != '.' and $f != 'remove.txt' and @is_readable($p = "$dir/$f")) {
				if (is_file($p)) {
					if (preg_match(';.*[.]' . $ext . '$;iS', $f)) {
						$files[] = substr($f, $skip, - (strlen($ext) + 1));
					}
				}
			}
			$nbfiles++;
		}
	}
	sort($files);
	return $files;
}
