<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * \~french
 * Classe Controle : affiche une interface de saisie pour une variable ACS
 * @param $composant : type de composant
 * @param $nic : numero d'instance de composant
 * @param $nom : nom de variable
 * @param $value : valeur de la variable
 * @param $param : parametres definis par le fichier composant.xml du composant
 *
 * \~english
 * Controle class : display a graphic interface to edit an ACS variable
 */
abstract class Controle {
	protected $composant;
	protected $nic;
	protected $nom;
	protected $value;
	protected $param;
	protected $var;
	protected $title;

	public function __construct($composant, $nic, $nom, $value, $param) {
		$this->composant = $composant;
		$this->nic = $nic;
		$this->nom = $nom;
		$this->value = $value;
		$this->param = $param;
		$this->var = 'acs' . ucfirst($composant) . $nic . $nom;
		$this->title = $this->var;
		if (isset($param['label']) && ($param['label'] != 'non')) {
			$help_src = $nom . 'Help';
			$help = $this->t($help_src);
			$this->title .= (($help != $help_src) ? "\n" . str_replace('"', '&quot;', $help) : '');
		}
	}
/**
 * \~french
 * Retourne la traduction spécifique au composant, ou sinon une traduction par
 * défaut, ou sinon, le texte.
 */
	public function t($txt) {
		return _T('acs:' . $this->composant . '_' . $txt);
	}

	abstract public function draw();
}

/**
 * \~french
 * Classe ctlColor : Choix de couleur
 *
 * \~english
 * ctlColor class : Color choice
 */
class ctlColor extends Controle {
	public function draw() {
		include_spip('plugins-dist/filtres_images/filtres/couleurs');
		if (substr($this->value, 0, 4) == '=acs') {
			$color = var_recursive($GLOBALS['meta'], substr($this->value, 1));
			if (!$color) { // Cas des variables de type "Bord", par exemple
				$color = var_recursive($GLOBALS['meta'], substr($this->value, 1) . '/Color');
			}
		} else {
			$color = $this->value;
		}
		if (!$color) {
			$color = '#ffffff';
		}
		return acs_vue('ctlColor', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->title,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'color' => $color,
			'value' => $this->value
		]);
	}
}

/**
 * \~french
 * Classe ctlImg : Choix d'image
 *
 * \~english
 * ctlImg class : image choice
 */
class ctlImg extends Controle {
	public function draw() {
		include_spip('inc/acs_mkdir_recursive');
		$path = $GLOBALS['ACS_CHEMIN'] . '/' . $this->param['chemin'];
		$help = $this->t($this->nom . 'Help');
		return acs_vue('ctlImg', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->title,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'path' => $path,
			'root' => _DIR_RACINE,
			'value' => $this->value,
			'err' => (!mkdir_recursive(_DIR_RACINE . $path) ? '*' : ''),
			'img' => _DIR_ACS . 'images/folder_image.png',
			'img_alt' => _T('adminacs:choix_image') . ' ' . $this->var,
			'img_help' =>  _T('adminacs:choix_image') . ' ' . $this->var  . (substr($help, -4) != 'Help' ? '&#13;' . $help : '')
		]);
	}
}

/**
 * \~french
 * Classe ctlBord : Choix de bordure
 *
 * \~english
 * ctlBord class : Border choice
 */
class ctlBord extends Controle {
	public function draw() {
		$b = acs_get($this->var);
		$bord = unserialize($this->value);
		if (is_array($bord)) {
			$largeur = $bord['Width'];
			$style = $bord['Style'];
			$couleur = $bord['Color'];
		}
		elseif (substr($b, 0, 1) === '=') {
			$largeur = '=';
			$style = '=';
			$couleur = $b;
		} else {
			$largeur = $style = $couleur = null;
		}
		$c_color = new ctlColor($this->composant, $this->nic, $this->nom . 'Color', $couleur, $this->param);
		$this->param['label'] = 'non';
		$this->param['taille'] = 8;
		$c_largeur = new ctlText($this->composant, $this->nic, $this->nom . 'Width', $largeur, $this->param);
		$c_style = new ctlStyleBord($this->composant, $this->nic, $this->nom . 'Style', $style, $this->param);
		return acs_vue('ctlBord', [
			'color' => $c_color->draw(),
			'largeur' => $c_largeur->draw(),
			'style' => $c_style->draw(),
		]);
	}
}

/**
 * \~french
 * Classe ctlLargeurBord : Choix d'une largeur de bordure OBSOLETE: will be removed in next versions
 *
 * \~english
 * ctlLargeurBord class : Border width choice OBSOLETE will be removed in next versions
 */
class ctlLargeurBord extends Controle {
	public function draw() {
		$ctl = new ctlText($this->composant, $this->nic, $this->nom, $this->value, $this->param);
		return $ctl->draw();
	}
}

/**
 * \~french
 * Classe ctlStyleBord : Choix d'un style	de bordure
 *
 * \~english
 * ctlStyleBord class : Border style choice
 */
class ctlStyleBord extends Controle {
	public function draw() {
		$style = $this->value;
		$styles = ['', 'none', 'solid', 'dashed', 'dotted', 'double', 'groove', 'ridge', 'inset', 'outset'];
		$styles_list = [];
		if (!in_array($style, $styles)) {
			$styles_list[$style] = [
					'title' => $style . ' (' . acs_get(substr($style, 1)) . ')',
					'selected' => ' selected'
				];
		}
		foreach ($styles as $st) {
			$styles_list[$st] = [
				'title' => _T('varacs:' . $st),
				'selected' => ($style == $st ? ' selected' : '')
			];
		}
		return acs_vue('ctlStyleBord', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->t('acs:bordstyle') . ' ' . $this->var,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'styles' => $styles_list
		]);
	}
}

/**
 * \~french
 * Classe ctlFontFamily : Choix d'une famille de fonte
 *
 * \~english
 * ctlFontFamily class : Font family choice
 */
class ctlFontFamily extends Controle {
	public function draw() {
		$families = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monotype'];
		$flist = [];
		foreach ($families as $ff) {
			$flist[$ff] = [
				'title' => $this->t('acs:' . $ff),
				'selected' => ($this->value == $ff ? ' selected' : '')
			];
		}
		return acs_vue('ctlFontFamily', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->var,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'ffamilies' => $flist
		]);
	}
}

/**
 * \~french
 * Classe ctlText : Saisie d'un texte
 *
 * \~english
 * ctlText class : short text
 */
class ctlText extends Controle {
	public function draw() {
		return acs_vue('ctlText', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->title,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'size' => isset($this->param['taille']) ? $this->param['taille'] : false,
			'value' => htmlspecialchars($this->value)
		]);
	}
}

/**
 * \~french
 * Classe ctlTextarea : Saisie d'un texte long
 *
 * \~english
 * ctlTextarea class : long text
 */
class ctlTextarea extends Controle {
	public function draw() {
		return acs_vue('ctlTextarea', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->var . (isset($this->help) ? ' ' . $this->help : ''),
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'lines' => (isset($this->param['lines']) ? $this->param['lines'] - 1 : 2),
			'value' => htmlspecialchars($this->value)
		]);
	}
}

/**
 * \~french
 * Classe ctlChoix : Choix
 *
 * \~english
 * ctlChoix class :	choice
 */
class ctlChoix extends Controle {
	public function draw() {
		if (!is_array($this->param['option'])) {
			return 'Pas d\'options pour ' . $this->nom;
		}
		$lchoix = [];
		foreach ($this->param['option'] as $option) {
			switch ($option) {
				case 'oui':
				case 'yes':
					$label = _T('varacs:oui');
					break;
				case 'non':
				case 'no':
					$label = _T('varacs:non');
					break;
				default:
					$choix = $this->nom . ucfirst($option);
					$label = $this->t($choix);
					// Sinon, on cherche si une traduction générique existe dans le module de langue varacs
					if ($label == $choix) {
						$label = _T('varacs:' . strtolower($option));
					}
			}
			$lchoix[$option] = acs_bouton_radio(
				$this->var,
				$option,
				$label,
				$this->value == $option
			);
		}
		return acs_vue('ctlChoix', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->var,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'liste_choix' => $lchoix
		]);
	}
}

/**
 * \~french
 * Classe ctlUse : Choix utiliser oui/non
 *
 * \~english
 * ctlUse class :	choice use or not (oui/non)
 */
class ctlUse extends Controle {
	public function draw() {
		$ctl = new ctlChoix($this->composant, $this->nic, $this->nom, ((acs_get('acs' . ucfirst($this->composant) . $this->nic . $this->nom) == 'oui') ? 'oui' : 'non'), ['option' => ['oui', 'non']]);
		$r = $ctl->draw();
		return $r;
	}
}

/**
 * \~french
 * Classe ctlKey : Choix d'un mot-clef
 *
 * \~english
 * ctlKey class : keyword choice
 */
class ctlKey extends Controle {
	public function draw() {
		include_spip('inc/texte'); // Requis pour fonction typo() utilisée dans la vue

		$this->value = unserialize($this->value);
		$vid_group = $this->value['Group'];
		$groups_query = sql_select('id_groupe, titre, ' . sql_multi('titre', "$spip_lang"), 'spip_groupes_mots', '', '', 'multi');
		while ($row_groupes = sql_fetch($groups_query)) {
			$groups[$row_groupes['id_groupe']] = typo($row_groupes['titre']);
		}
		if ($vid_group) {
			$keys_query = sql_select('titre, ' . sql_multi('titre', "$spip_lang"), 'spip_mots', 'id_groupe=' . $vid_group, '', 'multi');
			while ($row_keys = sql_fetch($keys_query)) {
				$keys[] = typo($row_keys['titre']);
			}
		}
		return acs_vue('ctlKey', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->var,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'group' => $this->value['Group'],
			'key' => $this->value['Key'],
			'groups' => $groups,
			'keys' => $keys,
		]);
	}
}

/**
 * \~french
 * Classe ctlKeyGroup : Choix d'un groupe de mots-clefs
 *
 * \~english
 * ctlKeyGroup class : keywords group choice
 */
class ctlKeyGroup extends Controle {
	public function draw() {
		// Requis pour fonction typo()
		include_spip('inc/texte');

		global $spip_lang;

		$vid_group = $this->value;
		$r = '<div align="' . $GLOBALS['spip_lang_right'] . '"><table><tr>';
		if (isset($this->param['label']) && ($this->param['label'] != 'non')) {
			$r .= '<td><label for="' . $this->var . '" title="' . $this->title . '"	class="acsLabel">' . $this->t($this->nom) . '</label>&nbsp;</td>';
		}
		$r .= '<td><select id="select_' . $this->var . '" name="' . $this->var . '" class="forml" title="' . $this->var . ($vid_group ? ' = ' . $vid_group : '') . '">';
		$r .= '<option value=""' . ($vid_group == '' ? ' selected' : '') . '></option>';
		$groups_query = sql_select('*, ' . sql_multi('titre', "$spip_lang"), 'spip_groupes_mots', '', '', 'multi');
		while ($row_groupes = sql_fetch($groups_query)) {
			$id_groupe = $row_groupes['id_groupe'];
			$titre_groupe = typo($row_groupes['titre']);
			$r .= '<option value="' . $id_groupe . '"' . ($id_groupe == $vid_group ? ' selected' : '') . '>' . $titre_groupe . '</option>';
		}
		$r .= '</select></td></tr></table></div>';
		return $r;
	}
}

/**
 * \~french
 * Classe ctlWidget : Choix d'un composant
 *
 * \~english
 * ctlWidget class :	choice of a component
 */
class ctlWidget extends Controle {
	public function draw() {
		$composants[''] = [
			'nom' => '',
			'selected' => $this->value == '' ? ' selected' : ''
		];
		foreach (composants_liste() as $type => $c) {
			foreach ($c['instances'] as $lnic => $cp) {
				if (($lnic == $this->nic) || ($cp['on'] !== 'oui')) {
					continue;
				}
				$composants[$type . ($lnic ? '-' . $lnic : '')] = [
					'nom' => ucfirst($type) . ($lnic ? ' ' . $lnic : ''),
					'selected' => ($this->value == $type . ($lnic ? '-' . $lnic : '') ? ' selected' : '')
				];
			}
		}
		return acs_vue('ctlWidget', [
			'varid' => $this->var,
			'varname' => $this->t($this->nom),
			'title' => $this->title,
			'label' => !isset($this->param['label']) || ($this->param['label'] != 'non'),
			'composants' => $composants,
			'ghost' => (in_array($this->value, array_keys($composants)) ? '' : '<a class="alert" title="' . $this->value . '">*</a>')
		]);
	}
}

/**
 * \~french
 * Classe ctlHidden : contrôle caché
 *
 * \~english
 * ctlHidden class : hidden control
 */
class ctlHidden extends Controle {
	public function draw() {
		return '<input type="hidden" name="' . $this->var . '" value="' . $this->value . '" />';
	}
}

// https://code.spip.net/@bouton_radio
function acs_bouton_radio($nom, $valeur, $titre, $actif = false, $on_click = '', $enable = true) {
	static $id_label = 0;

	if (strlen($on_click) > 0) {
		$on_click = " onclick=\"$on_click\"";
	}
	$texte = "<input type='radio' name='$nom' value='$valeur' id='radio_$id_label'$on_click";
	if ($actif) {
		$texte .= ' checked="checked"';
		$titre = '<b>' . $titre . '</b>';
	}
	$texte .= ($enable ? '' : ' disabled') . " />&nbsp;<label for='radio_$id_label'>$titre</label>";
	$id_label++;
	return $texte;
}
