<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne les composants et les pages du squelette qui utilisent l'instance $nic du composant $c
 *
 * @param string $c : classe de composant
 * @param int $nic : numero d'instance
 */
function composant_infos($c, $nic) {
	include_spip('inc/filtres');
	require_once(_DIR_ACS . 'inc/composant/classAdminComposant.php');

	$r = '<br>';

	// On calcule la liste de toutes les instances de composants actifs
	$choix = [];
	foreach (composants_liste() as $class => $composant) {
		foreach ($composant['instances'] as $instance => $params) {
			if ($params['on'] == 'oui') {
				array_push($choix, $class . ($instance != 0 ? '-' . $instance : ''));
			}
		}
	}

	if (count($choix) == 0) {
		ajax_retour($r . '<div class="alert">' . _T('adminacs:config_not_found') . '</div>');
	}

	// On établit la liste de toutes les variables ACS ayant pour valeur le nom du composant $c.$nic
	$ca = [];
	foreach ($GLOBALS['meta'] as $k => $v) {
		if ((substr($k, 0, 3) == 'acs') && ($v == $c . ($nic ? '-' . $nic : ''))) {
			if (in_array($v, $choix)) {
				array_push($ca, substr($k, 3));
			}
		}
	}

	// On retourne la liste de tous les composants qui contiennent ce composant
	if (count($ca)) {
		$lv = liste_variables();
		if (is_array($lv)) {
			$r .= '<span class="onlinehelp">' . _T('adminacs:used_in') . '</span><br>';
			foreach ($ca as $var) {
				if (isset($lv[$var]['c'])) {
					$pc = $lv[$var]['c'];
					$pnic = $lv[$var]['nic'];
					if (acs_get('acs' . ucfirst($pc) . $pnic . 'Nom')) {
						$pnom = acs_get('acs' . ucfirst($pc) . $pnic . 'Nom');
					} else {
						$pnom = ucfirst($pc) . (isset($pnic) ? $pnic : '');
					}
					$r .= '&nbsp;&nbsp;&nbsp;<a class="nompage acsLabel" href="' . generer_url_ecrire('acs', '&onglet=composants&composant=' . $pc . ($pnic ? '&nic=' . $pnic : '')) . '" title="acs' . $var . '">' . $pnom . '</a><br>';
				}
			}
		}
	}

	// On crée une instance :
	$composant = new AdminComposant($c, $nic);

	// On cherche toutes les pages qui contiennent cette instance du composant
	$l = $composant->listePages();
	if ($l) {
		$r .= '<hr />' . $l;
	}

	// On récupère les dépendances dans un tableau ordonné
	// avec les	plugins requis d'abord, puis les composants :
	if (isset($composant->necessite['plugin'])) {
		$necessite = '';
		foreach ($composant->necessite['plugin'] as $nec) {
			if (isset($nec['nom'])) {
				$npr = $nec['nom'];
				$vr = $nec['compatibilite'];
			} elseif (isset($nec['id'])) { // Compatibilite avec anciennes versions
				$npr = $nec['id'];
				$vr = $nec['version'];
			}
			$current_version = $min_version = $max_version = '?';
			$class_span = ' class="alert"';
			$class_v = '';
			if (acs_get_from_active_plugin(strtoupper(trim($npr)))) {
				$get_version = $npr . '_version';
				if (is_callable($get_version)) { // la fonction existe pour spip et pour acs
					$current_version = $get_version();
				} elseif ($f = chercher_filtre('info_plugin')) { // pour les plugins sans fonction plugin_version()
					if (is_callable($f)) {
						$current_version = $f($npr, 'version');
					}
				}
				$version = substr($vr, 1, -1);
				$version = explode(';', $version);
				$min_version = $version[0];
				$max_version = $version[1];
				if (version_compare($min_version, $current_version, '<=')) {
					$class_span = '';
					$class_v = 'acs_dep_ok';
				}
			}
			$necessite .= '<li><span"' . $class_span . '">';
			if ($npr == 'spip') {
				$necessite .= _T('plugin_necessite_spip', ['version' => $min_version]);
			} else {
			$necessite .= _T('plugin_necessite_plugin', ['plugin' => '<b>' . $npr . '</b>', 'version' => $min_version]);
			}
			$necessite .= '</span> (<b class="' . $class_v . '">' . $current_version . '</b>)</li>';
		}
	}
	if (isset($composant->necessite['composant'])) {
		foreach ($composant->necessite['composant'] as $nec) {
			$ncr = $nec['nom'];
			$vr = $nec['compatibilite'];
			$cr = new AdminComposant($ncr);
			$current_version = $cr->version;
			if (!$current_version) {
				$current_version = '?';
			}
			$version = substr($vr, 1, -1);
			$version = explode(';', $version);
			$min_version = $version[0];
			$max_version = $version[1];
			if (version_compare($min_version, $current_version, '>')) {
				$class = 'alert';
				$class_v = '';
			}
			else {
				$class = '';
				$class_v = 'acs_dep_ok';
			}
			$necessite .= '<li><span class="' . $class . '">' .
					_T('adminacs:composant') . ' <a href="' . generer_url_ecrire('acs', 'onglet=composants&composant=' . $ncr) . '">' . $ncr . '</a> ' . $min_version .
					'</span>	(<b class="' . $class_v . '">' . $current_version . '</b>)</li>';
		}
	}
	if ($necessite) {
		$r .= '<hr /><div>' .
		_T('adminacs:require', ['class' => $composant->nom, 'version' => $composant->version]) . ' :<br><ul style="list-style-type: disc;list-style-position: inside;">' . $necessite . '</ul></div>';
	}

	return $r;
}
