<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne des informations détaillées sur une page
 * Utilise le cache ACS
 */

include_spip('inc/page/page_source');
include_spip('inc/acs_vue');
include_spip('inc/presentation');

function acs_page_get_infos($page, $mode, $detail) {
	include_spip('inc/acs_cache');
	$mode_source = ($mode == 'source');
	$mode = $mode_source ? '_source' : '_infos' . $detail;
	$r = acs_cache(
		'page_get_infos',
		'pg_' . urlencode($page) . $mode,
		[$page, $mode_source, $detail]
	);

	// Si le fichier a été modifié depuis la mise en cache, on force le recalcul
	$pg = find_in_path($page . '.html');
	acs_log('pg=' . $pg . ' $GLOBALS[\'dossier_squelettes\']=' . $GLOBALS['dossier_squelettes']);
	$pg_derniere_modif = filemtime($pg);
	if ($r[2] < $pg_derniere_modif) {
		$r = acs_cache(
			'page_get_infos',
			'pg_' . urlencode($page) . $mode,
			[$page, $mode_source, $detail],
			true
		);
	}
	return $r[0];
}

// renvoie un widget avec les options d'affichage d'une page
function page_modes($page, $mode_source, $detail) {
	// Plieur
	if ($detail && ($detail > 1)) {
		$on = true;
		$detail = '';
	}
	else {
		$on = false;
		$detail = '&detail=2';
	}
	// Mode schema / source
	$link = '<a style="color: white" title="' . $page . '" href="?exec=acs&onglet=pages&pg=' . $page;
	if ($mode_source) {
		$lblsrc = '<a id="mode_source" name="srcon" title="' . $page . '"><b>' . _T('adminacs:source') . '</b></a>';
		$lblsch = $link . '" id="mode_schema">' . _T('adminacs:schema') . '</a>';
	}
	else {
		$lblsrc = $link . '&mode=source" id="mode_source" name="srcoff">' . _T('adminacs:source') . '</a>';
		$lblsch =	'<a id="mode_schema"><b>' . _T('adminacs:schema') . '</b></a>';
	}
	// Rendu
	$r = '';
	$r .= $lblsrc . ' / ' . $lblsch . '&nbsp;';
	$r .= acs_plieur(
		'plieur_spip_params',
		'spip_params',
		generer_url_ecrire('acs', '&onglet=pages&pg=' . $page . $detail),
		$on
	);
	return $r;
}

function page_get_infos($page, $mode_source = false, $detail = '') {
	include_spip('inc/acs_widgets');

	$pg = find_in_path($page . '.html');
	$pg_derniere_modif = filemtime($pg);
	$page_content = @file_get_contents($pg);
	$includes = analyse_page($page_content, $mode_source);
	$r = '';
	if (!$mode_source && (count($includes['vars']) > 0)) {
		$r .= '<div class="onlinehelp">' .
			_T('adminacs:variables') .
			' : ' .
			implode(' ', $includes['vars']) .
			'</div><br>';
		$infos = true;
	}
	ksort($includes['tags']);
	if ($mode_source) {
		$r .= '<div class="onlinehelp">' .
			_T('adminacs:source_page') .
			' : </div><div style="line-height: 1.5em;">';
		$dejalu = 0;
		$srcol = [];
		foreach ($includes['tags'] as $debut => $tag) {
			//echo '<br/><div class="alert">¤ '.$tag['type'].' : '.$debut.'-'.$tag['fin'].'</div><br/>'
						//.htmlspecialchars(substr($page_content, $debut, $tag['fin'] - $debut)).'' // debug code

			$source_tag = substr($page_content, $debut, $tag['fin'] - $debut);
			if ($tag['contenu']) {
				$spip_tag = $tag['contenu'];
			}
			else {
				$spip_tag = $source_tag;
			}
			if ($debut > $dejalu) {
				$srcol[] = affiche_source(
					$page_content,
					$dejalu,
					$debut - $dejalu
				) . '<span class="col_' . $tag['type'] . '">' . affiche_source($spip_tag) . '</span>';
				$dejalu = $tag['fin'];
			}
			else {	// il faut réécrire par dessus la dernière balise acs-spip (on espère que c'est la dernière ! ;-)
				$pos = strpos(end($srcol), $source_tag);
				if ($pos !== false) {
					$db = count($srcol) - 1;
					$srcol[$db] = substr($srcol[$db], 0, $pos) .
						'<span class="col_' . $tag['type'] . '">' . $spip_tag . '</span>' .
						substr($srcol[$db], $pos + strlen($source_tag));
				}
			}
		}
		$pagid = str_replace('/', '_slash_', str_replace('-', '_tiret_', $page));
		$src = '<div class="spip_source">';
		$src .= ' ' . nl2br(implode('', $srcol));
		if ($dejalu < strlen($page_content)) {
			$src .= affiche_source($page_content, $dejalu);
		}
		$src .= '</div>';
		$r .= $src;
	}
	else {
		if (count($includes['tags'])) {
			$r .= '<div class="onlinehelp">' . _T('adminacs:structure_page') . ' : </div><div class="structure_page">';
			$schema = '';
			foreach ($includes['tags'] as $debut => $tag) {
				if (isset($tag['contenu'])) {
					$schema .= ' ' . $tag['contenu'];
				}
			}
			$r .= $schema;
		}
		else {
			$no_infos = true;
		}
	}
	$r .= '</div><br>';

	if (isset($no_infos)) {
		$r = '<div>' . _T('adminacs:page_rien_a_signaler') . '</div><br>';
	}
	$r .= '<table width="100%"><tr><td><span class="onlinehelp">' .
		_T('adminacs:source') .
		' : </span><a class="lien_source" href="?exec=acs&onglet=pages&pg=' .
		$page .
		'&mode=source" title="' . read_perms($pg) . '">' .
		substr($pg, 3) .
		'</a></td><td style="text-align:' . $GLOBALS['spip_lang_right'] . '">' .
		_T('adminacs:acsdernieremodif') . ' ' . date('Y-m-d H:i:s', $pg_derniere_modif) .
		'</td></tr></table>';

	return acs_vue('acsBox', [
		'titre' => '<label class="acsLabel" title="' . _T('adminacs:pg_help') . '">' . _T('adminacs:page') . '</label> ' . $page,
		'titre2' => page_modes($page, $mode_source, $detail),
		'contenu' => $r,
		'class' => '',
		'style' => '',
		'icon' => _DIR_ACS . 'images/page-24.gif'
	]);
}

function affiche_source($txt, $debut = 0, $longueur = 0) {
	if ($longueur == 0) {
		$longueur = strlen($txt) - $debut;
	}

	$txt = substr($txt, $debut, $longueur);
	$txt = preg_replace(['/</', '/>/'], ['&lt;', '&gt;'], $txt);

// Indentation et sauts de lignes
	$txt = explode("\n", $txt);
	foreach ($txt as $n => $line) {
		$txt[$n] =	preg_replace('/ (?= )/s', '&nbsp;', $line);
	}
	$txt = implode('<br>', $txt);
	return $txt;
}

function read_perms($f) {
	$perms = fileperms($f);
	return substr(decoct($perms), 3);
}
