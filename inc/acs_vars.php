<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/texte'); // pour fonction couper()

function acs_vars_gauche() {
	$r = '';
	$vft = [
		'' => _T('adminacs:variables_all'),
		'def' => _T('adminacs:variables_defined'),
		'undef' => _T('adminacs:variables_undefined'),
	];
	foreach ($vft as $vf => $t) {
		if (_request('vf') == $vf) {
			$r .= '<b>';
		}
		$r .= '<a href="' . generer_url_ecrire('acs', 'onglet=vars') . '&vf=' . $vf . '">' . $t . '</a><br>';
		if (_request('vf') == $vf) {
			$r .= '</b>';
		}
	}
	return acs_vue('acsBox', [
		'titre' => _T('adminacs:variables'),
		'titre2' => '<img src="' . _DIR_ACS . 'images/info.png" alt="&#9432;" />',
		'contenu' => '<div>' . $r . '</div>',
		'class' => '',
		'style' => '',
		'icon' => _DIR_ACS . '/images/vars-24.gif'
	]);
}

function acs_vars($filter) {
	require_once(_DIR_ACS . 'inc/acs_widgets.php');

	$cv = composants_variables();
	$acs_vars = [];
	foreach ($GLOBALS['meta'] as $vn => $val) {
		if (substr($vn, 0, 3) === 'acs') {
			$acs_vars[$vn] = $val;
		}
	}
	$r = liste_widgets(false);
	$r .= '<table class="acs_vars_list" frame="void" rules="cols" cellpadding="2" cellspacing="0">';
	$r .= '<thead><tr><th class="verdana1"><b>' . _T('adminacs:variable') . "</b></th>\n<th class='verdana1'><b>" . _T('adminacs:valeur') . "</b></th>\n</tr></thead>\n";
	foreach ($cv as $c => $p) {
		foreach (composant_instances($c) as $nic) {
			$i = 0;
			foreach ($p['vars'] as $var => $vp) {
				if (substr($var, 0, 6) == '#over#') {
					$v = substr($var, 6);
					$before = '<u>';
					$after = '</u>';
				}
				else {
					$v = $var;
					$before = '';
					$after = '';
				}
				$varname = 'acs' . ucfirst($c) . $nic . $v;
				if ($filter == 'def' && !acs_get($varname)) {
					unset($acs_vars[$varname]);
					continue;
				} elseif ($filter == 'undef' && acs_get($varname)) {
					unset($acs_vars[$varname]);
					continue;
				}
				$bgcolor = alterner($i++, '#eeeeee', 'white');
				$r .= '<tr style="background: ' . $bgcolor . '; vertical-align: top;"><td class="verdana2"><a href="?exec=acs&onglet=composants&composant=' . $c . ($nic ? '&nic=' . $nic : '') .
					'" class="nompage">' .
					$before .
					'<span style="color:#8d8d8f">acs' . ucfirst($c) . $nic . '</span>' .
					$v . $after . '</a></td>' .
					'<td class="crayon var-' . $c . '_' . $v . '-' . ($nic ? $nic : 0) . ' type_pinceau crayon_' . $c . $v . ' arial2">' .
					affiche_variable($vp['type'], $varname) .
					'</td></tr>';
				unset($acs_vars[$varname]);
			}
			if ($i) {
				$r .= '<tr class="vsep"><td colspan="2" ></td></tr>';
			}
		}
	}
	$r .= '</table>';
	if (count($acs_vars) > 0) {
		ksort($acs_vars);
		$key = md5(implode('', $acs_vars));
		if (_request('delete_ghost_vars', false) === $key) {
			foreach ($acs_vars as $vn => $val) {
				acs_unset($vn);
			}
		} else {
			$r .= '<div class="titrem center acsLabel" title="' .  _T('adminacs:variables_fantomes_help') . '">' . _T('adminacs:variables_fantomes') . '<span class="nav" style="float:right"><a href="' . generer_url_ecrire('acs', 'onglet=vars&delete_ghost_vars=' . $key) . '" title="' . _T('adminacs:effacer_variables_fantomes') . '" class="acsLabel"><img src="' . _DIR_ACS . '/images/supprimer.gif" alt="x"></a></span></div>';
			foreach ($acs_vars as $vn => $val) {
				$r .= $vn . ' = ' . $val . '<br>';
			}
		}
	}
	return acs_vue('acsBox', [
		'titre' => _T('adminacs:variables_all'),
		'titre2' => false,
		'contenu' => $r,
		'class' => '',
		'style' => '',
		'icon' => _DIR_ACS . '/images/vars-24.gif'
	]);
}

/**
 * Affiche une vue de la variable
 * @param $varname
 */
function affiche_variable($type, $varname) {
	$r = '';
	if (acs_get($varname)) {
		$var = acs_get($varname);
		if (is_array(unserialize($var))) {
			foreach (unserialize($var) as $svn => $svv) {
				$r .= affiche_variable_2($svv, $type);
			}
		} else {
			$r .= affiche_variable_2($var, $type);
		}
	}	else {
		$r .= '<span style="color:darkviolet">' . _T('adminacs:undefined') . '</span> ';
	}
	return $r;
}

function affiche_variable_2($v, $type) {
	$r = '';
	if ($type == 'color') {
		if (substr($v, 0, 4) == '=acs') {
			$color = var_recursive($GLOBALS['meta'], substr($v, 1));
		} else {
			$color = $v;
		}
		$r = '<div class="acsVarColorBlock" style="background:' . $color . '"></div>';
	}
	$r = couper(htmlspecialchars($v), 150) . ' ' . $r;
	return $r;
}
