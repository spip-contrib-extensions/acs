<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Page composant
 */
	require_once(_DIR_ACS . 'inc/composant/classAdminComposant.php');

function composants($c) {
	$cl = composants_liste();
	$over = '';
	if (isset($cl[$c->class]['over'])) {
		$over = '<td><img src="' . _DIR_ACS . 'images/over.gif" alt="over" title="' . _T('ecrire:icone_squelette') . ' ' . acs_get('ACS_OVER') . '" /></td>';
	}
	$nom = ucfirst(str_replace('_', ' ', $c->class));

	// Si le composant est instanciable, on affiche de quoi gérer l'instanciation
	if ($c->instanciable) {
		$instances = '<td>' . instance_create($c) . '</td><td>' . instance_select($c) . '</td>';
	}
	else {
		$instances = '';
	}

	// Affichage de la box de gestion du composant
	$h = $c->t('help');
	if (is_trad($h, 'help')) {
		$box_title = '<label class="acsLabel" title="' . str_replace('"', '&quot;', $h) . '">' .
			$nom . ' ' . $c->nic . '</label>';
	} else {
		$box_title = $nom . ' ' . $c->nic;
	}
	return acs_vue('acsBox', [
		'titre' => '<table width="100%"><tr><td width="99%">' . $box_title . '</td><td>' . $c->tabs() . '</td>' .
			$instances .
			$over .
			'</tr></table>',
		'titre2' => false,
		'contenu' => $c->edit() . (count($c->errors) ? '<div class="alert">' . implode('<br>', $c->errors) . '</div>' : ''),
		'class' => '',
		'style' => '',
		'icon' => $c->icon
	]);
}

function instance_select($c) {
	$r = '';
	$instances = composant_instances($c->class);
	if (is_array($instances) && count($instances) > 1) {
		$vp = 'acs' . ucfirst($c->class);
		$r = '<select name="nic" onchange=submit()>';
		foreach ($instances as $id) {
			$v = $vp . $id . 'Nom';
			$title = acs_get($v) ? ' title="' . acs_get($v) . '"' : '';
			$r .= '<option value="' . $id . '"' . ($id == $c->nic ? ' selected' : '') . $title . '>' . $id . '</option>';
		}
		$r .= '</select>';
		$r .= "<input type='hidden' name='exec' value='acs' />" .
		'<input type="hidden" name="composant" value="' . $c->class . '" />' .
		'<input type="hidden" name="onglet" value="composants" />';
		$r = '<td><form action="">' . $r . '<noscript></td><td><input type="submit" value="' . _T('bouton_valider') . '"></noscript></form></td>';
		$r .= '<td>' . instance_delete($c) . '</td>';
	}
	return $r;
}

function instance_create($c) {
	$new_nic = $c->nextInstance();
	$post_url = generer_url_ecrire('acs', 'onglet=composants&composant=' . $c->class . '&nic=' . $new_nic);
	return '<form method="post" action="' . $post_url . '" id="form_instance_create" onSubmit="return instance_create(\'' .
		$new_nic .
		'\');"></td><td><noscript><input type="text" name="nic" size="4" maxlength="4" /></td><td></noscript><input type="image" src="' .
		_DIR_ACS .
		'images/composant-creer.gif" title="' .
		_T('adminacs:creer_composant') . '" />
		<input type="hidden" name="create_instance_' . md5('acs' . ucfirst($c->class) . $new_nic) . '" value="create" />
		</form>';
}

function instance_delete($c) {
	$msg = str_replace("'", "\'", _T('adminacs:del_composant_confirm', ['c' => $c->class, 'nic' => $c->nic]));
	return '<form method="post" action="' . $c->urledit . '" onSubmit="return instance_delete(\'' . $msg . '\');"><input type="image" src="' . _DIR_ACS . 'images/composant-del.gif" title="' . _T('adminacs:del_composant') . '" />
		<input type="hidden" name="del_composant_' . md5($c->fullname) . '" value="delete" />
		</form>';
}
