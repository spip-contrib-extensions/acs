<?php

/**
 * function acs_vue(template, context)
 * Tiny uncached "no-template" system. Faster than any template system.
 *
 * @param string $tpl path of the php template
 * @param array $context variables
 */
function acs_vue($tpl, $context) {
	$file = find_in_path('vues/' . $tpl . '.html');
	if (!$tpl || !is_readable($file)) {
		acs_log('Vue "' . $tpl . '" not found or not readable as file ' . $file, _LOG_DEBUG);
		return '';
	}
	extract($context); // Extract the vars to local namespace
	ob_start(); // Start output buffering
	include($file); // Include the file
	$r = ob_get_contents(); // Get the contents of the buffer
	ob_end_clean(); // End buffering and discard

	return $r; // return the content
}

/**
 * Crée un lien image plieur/déplieur jQuery pour les éléments de la classe $classe
 * Utilise l'url si pas de jQuery ou pas de javascript (soft downgrade)
 *
 * Classes définies:
 * plieur : lien a href
 */
function acs_plieur($id_plieur, $classe_a_plier, $url, $on = false, $onclick = false, $texte = '') {
	if ($onclick) {
		$onclick = ' onclick="' . $onclick . '"';
	}
	return '<a href="' . $url . '" id="' . $id_plieur . '" class="acs_plieur' . ($on ? ' plon' : '') . '" name="plieur_' . $classe_a_plier . '" title="' .  _T('info_deplier') . '"' . $onclick . '>' . ($texte ? ' ' . $texte . ' ' : '') . '</a>';
}
