<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function acs_var_vals($var) {
	include_spip('inc/presentation'); // include spip function ajax_retour()

	$lv = liste_variables();
	$vn = substr($var, 3);
	$c = ucfirst($lv[$vn]['c']);
	$strlen_nic = isset($lv[$vn]['nic']) ? strlen($lv[$vn]['nic']) : 0;
	$jvn = substr($vn, strlen($c) + $strlen_nic);
	$r = [];
	$ret = '';
	foreach ($lv as $v => $ctn) {
		if ((strcmp($c, substr($v, 0, strlen($c))) === 0) && (strcmp($jvn, substr($v, - strlen($jvn))) === 0) && (strlen(acs_get('acs' . $v)) > 0) && (!isset($r[acs_get('acs' . $v)]))) {
			$recv = var_recursive($GLOBALS['meta'], 'acs' . $v);
			$metav = acs_get('acs' . $v);
			$r[$metav] = ucfirst($ctn['c']) . $ctn['nic'] . $jvn . ' ' .
				($metav <> $recv ? ' ' . $metav : '') .
				($recv ? ' = ' . $recv : '');
		}
	}
	ksort($r);
	foreach ($r as $v => $rv) {
		$ret .= '<li class="acsLabel" title="' . $rv . '" onclick="jQuery(\'#' . $var . '\').val(\'' . $v . '\');jQuery(\'#av_' . $var . '\').toggle();">' . $v . '</li>';
	}
	if (strlen($ret) > 0) {
		$ret = '<ul>' . $ret . '</ul>';
	}
	ajax_retour($ret);
}
