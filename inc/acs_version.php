<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Version - Lue dans la variable meta que spip a écrit
function acs_version() {
	static $v;
	if (!$v) {
		$v = acs_get_from_active_plugin('ACS', 'version');
	}
	return $v;
}
