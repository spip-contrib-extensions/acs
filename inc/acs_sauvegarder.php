<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne le nom par défaut du fichier de sauvegarde
 * @return string filename
 */
function acs_nom_sauvegarde() {
	return 'acs' . date('ymdHi', acs_get('ACS_MAJ'));
}
/**
 * Sauvegarde de la configuration ACS du site
 * @param string $nom_sauvegarde
 * @return array
 */
function acs_sauvegarder($nom_sauvegarde) {
	$repertoire = _DIR_DUMP . 'acs/';
	// si le répertoire n'existait pas, on le crée
	if (!is_writable($repertoire)) {
		include_spip('inc/acs_mkdir_recursive');
		if (!mkdir_recursive($repertoire)) {
			$err = _T('dump:avis_probleme_ecriture_fichier', ['fichier' => $repertoire]);
			acs_log('inc/acs_sauvegarder : ' . $err, _LOG_INFO);
			return ['message_erreur' => $err];
		}
	}
	require_once(_DIR_ACS . 'inc/acs_version.php');
	$filename = $repertoire . $nom_sauvegarde . '.php';
	$meta = $GLOBALS['meta'];
	$cv = composants_variables();
	foreach (liste_variables() as $vname => $var) {
		$vn = 'acs' . $vname;
		if (isset($meta[$vn]) && $meta[$vn] !== '') {
			$cp = $var['c'];
			$ci = $var['nic'];
			$vname = substr($vname, strlen($cp) + strlen($ci));
			$default = $cv[$cp]['vars'][$vname]['valeur'];
			if ($meta[$vn] !== $default) {
				$file .= "'$vn' => '" . str_replace("'", "\'", $meta[$vn]) . "',\n";
			}
		}
	}
	if ($file) {
		$file = '<?php # backup of ' . $meta['ACS_SET'] . "\n\$def = array(\n" .
				"'ACS_VERSION' => '" . acs_version() . "',\n" .
				"'ACS_SET' => '" . $meta['ACS_SET'] . "',\n" .
				($meta['ACS_OVER'] ? "'ACS_OVER' => '" . $meta['ACS_OVER'] . "',\n" : '') .
				"'ACS_VOIR_PAGES_COMPOSANTS' => '" . $meta['ACS_VOIR_PAGES_COMPOSANTS'] . "',\n" .
				"'ACS_VOIR_PAGES_PREVIEW' => '" . $meta['ACS_VOIR_PAGES_PREVIEW'] . "',\n" .
				$file .
				");\n";
		if (ecrire_fichier($filename, $file)) {
			acs_log('inc/acs_sauvegarder : ' . $filename, _LOG_INFO);
			return ['message_ok' => _T('adminacs:sauvegarde_ok')];
		}
	}
	return ['message_erreur' => _T('dump:erreur_taille_sauvegarde', ['fichier' => $filename])];
}
