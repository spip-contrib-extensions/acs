<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Makes directory, returns TRUE if exists or made
 *
 * @param string $pathname The directory path.
 * @return boolean returns TRUE if exists or made or FALSE on failure.
 */
function mkdir_recursive($pathname) {
		@is_dir(dirname($pathname)) || mkdir_recursive(dirname($pathname));
		if (@is_dir($pathname)) {
			return true;
		}
		acs_log('mkdir_recursive(' . $pathname . ')', _LOG_DEBUG);
		return @mkdir($pathname);
}
