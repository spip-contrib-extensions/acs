<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/acs_load_vars');

/**
 * Restaure la configuration ACS du site
 * @param string $fichier
 * @return array
 */
function acs_restaurer($fichier) {
	$file = _DIR_DUMP . 'acs/' . basename($fichier); // securite
	if (!is_file($file)) { // Ultime securite
		return ['message_erreur' => _T('adminacs:err_fichier_absent', ['file' => joli_repertoire($file)])];
	}
	acs_reset_vars();
	$r = acs_load_vars($file);
	if ($r == 'ok') {
		return ['message_ok' => _T('adminacs:restored', ['file' => '"' . basename($fichier, '.php') . '"'])];
	}	else {
		return ['message_erreur' => $r];
	}
}
