<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Initialisation des variables ACS, à l'installation et lors d'une restauration
 *
 * @param string $from chemin du thème
 */
function acs_load_vars($from) {
	if (is_readable($from)) {
		include $from;
	}
	else {
		return 'unable to read ' . $from;
	}
	require_once(_DIR_ACS . 'inc/acs_version.php');
	if (is_array($def)) {
		// Cette ACS_VERSION est la version lors de la sauvegarde : NE PAS LA RESTAURER !
		// todo : gérer les version mismatches
		$dversion = $def['ACS_VERSION'];
		unset($def['ACS_VERSION']);

		foreach ($def as $var => $value) {
			if (is_array($value)) {
				serialize($value);
			}
			// on n'écrase pas les valeurs existantes lors d'une installation / reinstallation du plugin
			if (!acs_get($var)) {
				acs_set($var, $value);
			}
		}

		// On lit les valeurs par défaut des variables de compôsants
		$cv = composants_variables();
		foreach (liste_variables() as $vname => $var) {
			$fullname = 'acs' . $vname;
			if (acs_get($fullname)) {
				continue;
			}
			$cp = $var['c'];
			$ci = $var['nic'];
			$vn = substr($vname, strlen($cp) + strlen($ci));
			$default = $cv[$cp]['vars'][$vn]['valeur'];
			if ($default) {
				acs_set($fullname, $default);
			}
		}

		acs_recalcul();

		acs_log('acs_load_vars(' . $from . ')', _LOG_DEBUG);
		if ($dversion == acs_version()) {
			return 'ok';
		}
		else {
			$upf = 'upgrade_set';
			$url = include_spip('upgrade/' . $upf);
			if ($url) {
				$upr = $upf($dversion, acs_version());
			} else {
				acs_log('function ' . $upf . '() not found in set.', _LOG_DEBUG);
				return 'ok';
			}
			$from = substr(basename($from), 0, -4);
			return 'version mismatch (' . $from . ' version ' . $dversion . ' vs ACS ' . acs_version() . ')<br>' . $upr;
		}
	}
	else {
		return 'no vars';
	}
}

function acs_reset_vars() {
	spip_query("delete FROM spip_meta where left(nom,3)='acs'");
	lire_metas();
	acs_log('Variables DELETED', _LOG_INFO_IMPORTANTE);
}
