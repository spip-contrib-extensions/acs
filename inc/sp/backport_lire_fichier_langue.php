<?php

// Compatibilité (fonction spip 4.1 inexistante en spip 3.2)
/**
 * Retourne les entrées d’un fichier de langue
 *
 * Les fichiers de langue retournent soit un array [ cle => valeur ],
 * soit peuplent une globale `$GLOBALS[$GLOBALS['idx_lang']]`.
 *
 * @return string Chemin du fichier de langue (un fichier PHP)
 * @return array<string, string>
 */
if (!function_exists('lire_fichier_langue')) {
	function lire_fichier_langue(string $fichier): array {
		$idx_lang_before = $GLOBALS['idx_lang'] ?? null;
		$idx_lang_tmp = ($GLOBALS['idx_lang'] ?? 'lang') . '@temporaire';
		$GLOBALS['idx_lang'] = $idx_lang_tmp;
		$idx_lang = include $fichier;
		$GLOBALS['idx_lang'] = $idx_lang_before;
		if (!is_array($idx_lang)) {
			if (isset($GLOBALS[$idx_lang_tmp]) and is_array($GLOBALS[$idx_lang_tmp])) {
				$idx_lang = $GLOBALS[$idx_lang_tmp];
			} else {
				$idx_lang = [];
				acs_log(sprintf('Fichier de langue incorrect : %s', $fichier), _LOG_ERREUR);
			}
			unset($GLOBALS[$idx_lang_tmp]);
		}
		return $idx_lang;
	}
}
