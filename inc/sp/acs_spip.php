<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonctions de traduction
 */
include_spip('inc/traduire');
require_once(_DIR_ACS . 'inc/sp/backport_lire_fichier_langue.php');

/**
 * Ajoute un fichier de langue de composant à la langue en cours
 *
 * @param string $c composant
 * @param string $dir (= "" ou "ecrire")
 */
function composant_ajouter_langue($c, $dir = '') {
	$lang = $GLOBALS['spip_lang'];
	if (!$lang) {
		$lang = _LANGUE_PAR_DEFAUT;
	}
	$idx = 'i18n_acs_' . $lang;
	$GLOBALS[$idx] ??= [];
	// Le mécanisme de traduction de SPIP implique que ce fichier peut être appelé à chaque traduction, donc on met en cache
	$trads = acs_cache(
		'acs_lire_langue',
		'c_' . $c . '_' . ($dir ? $dir . '_' : '') . $lang,
		[$c, $lang, $dir],
		false,
	);
	$GLOBALS[$idx] = array_merge($GLOBALS[$idx], $trads[0]);
}
/**
 * Cherche si le composant a un fichier de traductions génériques
 * définies dans lang/varacs.php, par exemple, ou dans un module de langue de SPIP)
 * et un fichier de langue dans la langue en cours ou dans la langue par défaut
 *
 * @param string $c composant
 * @param string $langfile fichier
 * @param string $lang langue
 * @param string $dir (= "" ou "ecrire")
 * @return array
 */
function acs_lire_langue($c, $lang, $dir) {
	$cla = [];
	$clpath = 'composants/' . $c . '/' . ($dir ? $dir . '/' : '') . 'lang/' . $c;
	// Cherche si le composant a un fichier de traductions génériques
	$langfile = find_in_path($clpath . ($dir ? '_' . $dir : '') . '.php');
	if ($langfile) {
		$cla = acs_lire_fichier_langue($c, $langfile);
	}
	// Cherche le fichier de langue du composant :
	$langfile = find_in_path($clpath . '_' . ($dir ? $dir . '_' : '') . $lang . '.php');
	if (!$langfile) {
		$langfile = find_in_path($clpath . '_' . ($dir ? $dir . '_' : '') . _LANGUE_PAR_DEFAUT . '.php');
		$lang = _LANGUE_PAR_DEFAUT;
	}
	if ($langfile) {
		$cla = array_merge($cla, acs_lire_fichier_langue($c, $langfile));
	}
	return $cla;
}

function acs_lire_fichier_langue($c, $langfile) {
	$cla = [];
	// Charge le fichier de langue
	$trads = lire_fichier_langue($langfile); // Appel fonction Spip
	// Affecte les traductions chargée au composant
	if ((array) $trads === $trads) {
		foreach ($trads as $k => $v) {
			$cla[$c . '_' . $k] = $v;
		}
	}
	else {
		acs_log('Error in acs_lire_fichier_langue(\'' . $c . '\', \'' . $langfile . '\'). Not a lang file.', _LOG_ERREUR);
	}
	return $cla;
}

/**
 * inc_taduire() override la fonction Spip pour ajouter les traductions de composants
 *
 * @param string $texte
 * @param string $lang
 * @return string
 */
function inc_traduire($texte, $lang) {
	if (!$texte) {
		return '';
	}
	if (substr($texte, 0, 4) !== 'acs:') {
		return inc_traduire_dist($texte, $lang);
	}
	$txt = substr($texte, 4);
	$c = get_composant($txt);
	// Traductions d'un composant
	if ($c && ($c !== $txt)) {
		if (_DIR_RESTREINT != '') {
			if (_request('action') == 'crayons_html') { // On ajoute les traductions pour les crayons
				composant_ajouter_langue($c, 'ecrire');
			}
			composant_ajouter_langue($c);
		}
		else {
			composant_ajouter_langue($c, 'ecrire');
		}
		$t = inc_traduire_dist($texte, $lang);
		if (is_trad($t, $txt)) {
			return $t;
		}
		$txt = substr($txt, strlen($c) + 1);
	}
	// On cherche une traduction générique dans lang/varacs_??.php
	$t = inc_traduire_dist('varacs:' . strtolower($txt), $lang);
	if (is_trad($t, $txt)) {
		return $t;
	}
	// On cherche une traduction dans SPIP, à défaut
	$t = inc_traduire_dist($txt, $lang);
	if (is_trad($t, $txt)) {
		return $t;
	}
	//acs_log('traduire ' . $texte . ': pas de traduction dans ' . ucfirst($c) . ' ni dans varacs pour "' . $txt .'"');
	return $txt;
}

// Teste si $t est une traduction de $txt
function is_trad($t, $txt) {
	// 3 tests, pour SPIP 2, 3, et 4
	return ($t && ($t != $txt) && ($t != str_replace('_', ' ', strtolower($txt))) && (substr($t, 0, 12) != '<blink style'));
}

function get_composant($txt) {
	static $vu = [];

	if (isset($vu[$txt])) {
		return $vu[$txt];
	}
	$debut = '';
	foreach (explode('_', $txt) as $part) {
		if (!$debut) {
			$debut = $part;
		} else {
			$debut .= '_' . $part;
		}
		if (in_array($debut, unserialize($GLOBALS['ACS_COMPOSANTS_CLASSES']))) {
			$vu[$txt] = $debut;
			return $vu[$txt];
		}
	}
}

/**
 * Balises SPIP
 */

function balise_ACS_VERSION($p) {
	$p->code = 'calcule_balise_acs_version()';
	$p->statut = 'php';
	$p->interdire_scripts = false;
	return $p;
}
function calcule_balise_acs_version() {
	require_once(_DIR_ACS . 'inc/acs_version.php');
	return acs_version();
}

function calculer_balise_pinceau($composant, $nic, $class = null) {
	$nic = $nic ? $nic : '0';
	return	'crayon composant-' . $composant . '-' . $nic . ' type_pinceau' . ( $class ? ' ' . $class : '');
}

function balise_PINCEAU($p) {
	$composant = interprete_argument_balise(1, $p);
	$nic = interprete_argument_balise(2, $p);
	$nic = $nic ? $nic : "'0'";
	$class = interprete_argument_balise(3, $p);
	$p->code = 'calculer_balise_pinceau(' . $composant . ', ' . $nic . ', ' . $class . ')';
	$p->statut = 'php';
	$p->interdire_scripts = false;
	return $p;
}

function acs_chemin($path = '', $type = false) {
	$path = _DIR_RACINE . $GLOBALS['ACS_CHEMIN'] . '/' . $path;
	// On retourne vide si la ressource n'est pas lisible
	if (!is_readable($path)) {
		return '';
	}
	// Sinon si la ressource n'est pas un fichier on retourne vide
	// sauf si on recherchait un sous-dossier ou la racine des ressources ACS
	elseif (($path != '') && ($type != 'dir') && is_dir($path)) {
		return '';
	}
	return $path;
}
/**
 * Retourne le chemin d'une ressource ACS ou vide si la ressource n'est pas
 * accessible au moins en lecture.
 * Usages: #ACS_CHEMIN, #ACS_CHEMIN{chemin_fichier}, #ACS_CHEMIN{chemin_dossier,dir}
 */
function balise_ACS_CHEMIN($p) {
	$arg = interprete_argument_balise(1, $p);
	$type = interprete_argument_balise(2, $p);
	$p->statut = 'php';
	$p->interdire_scripts = false;
	if (is_null($arg)) {
		$p->code = 'acs_chemin()';
	}
	elseif (is_null($type)) {
		$p->code = "acs_chemin($arg)";
	}
	else {
		$p->code = "acs_chemin($arg, $type)";
	}
	return $p;
}

/** VAR = balise CONFIG de SPIP etendue, qui remplaçe les variables ACS par leur valeur, récursivement.
 * admet deux arguments : nom de variable, valeur par defaut si vide
 * syntaxe : #VAR{acsComposantVariable} ou #VAR{acsComposantVariable, valeur_par_defaut}
 * Lorsque la valeur d'une variable commence par le signe "=",
 * cette valeur est interprétée comme une référence récursive à une autre variable.
 */
function balise_VAR($p) {
	$var = interprete_argument_balise(1, $p);
	$sinon = interprete_argument_balise(2, $p);
	if (!$var) {
		$p->code = '""'; // cas de #VAR sans argument
	} else {
		$p->code = 'var_recursive($GLOBALS["meta"], ' . $var . ')';
		if ($sinon) {
			$p->code = 'sinon(' . $p->code . ',' . $sinon . ')';
		}
		else {
			$p->code = '(' . $p->code . ')';
		}
	}
	$p->interdire_scripts = false;
	return $p;
}

/**
 * Retourne headers, css ou javascripts de tous les composants
 */
function balise_COMPOSANTS_CODE($p) {
	$typeh = interprete_argument_balise(1, $p);
	$typeh = substr($typeh, 1, strlen($typeh) - 2);
	$p->code = 'composants_code("' . $typeh . '")';
	$p->statut = 'php';
	$p->interdire_scripts = false;
	return $p;
}

/**
 * Retourne css ou javascripts de tous les composants actifs du set,
 * concaténés.
 * @param string $type : css, javascript
 * @return string
 */
function composants_code($type) {
	// le retour de composants_liste() est statique,	mis en cache,
	// et tient compte de l'override éventuel.
	if (is_array(composants_liste())) {
		$r = '';
		foreach (composants_liste() as $class => $cp) {
			$nbi = 0;
			$rc = $ri = $rf = '';
			foreach ($cp['instances'] as $nic => $c) {
				if ($c['on'] != 'oui') {
					continue;
				}
				$nbi++;
				// On cherche les css d'instances de composants utilisés
				if (strtolower($type) == 'css') {
					$filepath = 'composants/' . $class . '/' . $class . '_instances.css';
					$file = find_in_path($filepath . '.html');
					if ($file) {
						$ri .= recuperer_fond($filepath, ['nic' => $nic, 'X-Spip-Cache' => 0]) . "\r";
						//acs_log('#COMPOSANTS_CODE(' . $type . '): ' . $filepath . ($nic ? ' instance ' . $nic : ''), _LOG_DEBUG);
					}
				}
			}
			if ($nbi > 0) {
				$type = strtolower($type);
				switch ($type) {
					case 'css':
						$f = $class . '.css';
						break;
					case 'javascript':
						$f = "javascript/$class.js";
						break;
					default:
						$f = $class . '_' . $type;
				}
				$filepath = 'composants/' . $class . '/' . $f;
				$file = find_in_path($filepath . '.html');
				if (!$file) {
					$file = find_in_path($filepath);
					if ($file) {
						$rc = file_get_contents($file);
						//acs_log('#COMPOSANTS_CODE(' . $type . '): ' . $file, _LOG_DEBUG);
					}
				}
				else {
					$rc = recuperer_fond($filepath, ['X-Spip-Cache' => 0]) . "\r";
					//acs_log('#COMPOSANTS_CODE(' . $type . '): ' . $filepath, _LOG_DEBUG);
				}
				// On cherche enfin la css d'override d'instances de composants (utile pour media queries, par ex.)
				if (strtolower($type) == 'css') {
					$filepath = 'composants/' . $class . '/' . $class . '_fin.css';
					$file = find_in_path($filepath . '.html');
					if ($file) {
						$rf = recuperer_fond($filepath, ['X-Spip-Cache' => 0]) . "\r";
						//acs_log('#COMPOSANTS_CODE(' . $type . '): ' . $filepath, _LOG_DEBUG);
					}
				}
				$r .= $rc . $ri . $rf;
			}
		}
	}
	// CSS et JS optionnels des sets de composants
	foreach (composants_sets() as $path => $set) {
		$file = _ACS_PLUGIN_SITE_ROOT . $path . '/' . $set['set'] . '.' . ($type === 'javascript' ? 'js' : 'css');
		if (is_file($file)) {
			$r .= file_get_contents($file);
		} else {
			if (is_file($file . '.html')) {
				$r .= recuperer_fond($file, ['X-Spip-Cache' => 0]) . "\r";
			}
		}
	}
	return $r;
}

/* Overide de la balise CACHE : permet de passer un paramètre à la balise SPIP
 * Les 2 paramètres de la balise #CACHE sont interprétés et passés à la balise
 * #CACHE de la dist.
 */
function balise_CACHE($p) {
	if (isset($GLOBALS['contexte']['cache'])) {
		$cache = explode(',', $GLOBALS['contexte']['cache']);
		$p->param[0][1][0]->texte = $cache[0];
		if (isset($cache[1])) {
			$p->param[0][1][1]->texte = $cache[1];
		}
	}
	return balise_CACHE_dist($p);
}
